# Design Motivation

Many of the calculations for smart tariffs and smart comparisons rely on time series data as input for then calculating overall metrics for comparison. For example, 30 minute electricity import from the grid matched to a 30 minute dynamic tariff, multiplied together to then represent units of money for that 30 minute period. Furthermore those time points are aggregated or *reduced* into more meaningful, larger time periods such as cost for the month or year.

It is the balance between accumlated cost, savings or other meaningful metric and optimising the fine grained event or observation that drives the build up that metric. They are in essence the same phenomenon, observed and managed at different levels. To a point, the closer you look at something the more it might be better understood, optimised and managed or at least locally optimal.

The big picture is looking at the tradeoffs, in our case sources of energy, time of use, carbon impact, price, usefulness of the energy -- and many more. Local optimality such as matching use to the cheapest rate by time of day will always be constrainted by the fact that there may not be demand for energy at that time. Further still,  such as the cost and capacity of a battery that provides demand flexibility demonstrate the need to consider higher order constraints.

There are then preferences, although it can be argued that preferences could just be metrics too. No one is really interested in poor customer service, shouldn't be maximising high carbon energy or wants high risk of uncertainty around price. These are all things that are either someone else's problem or are good enough on a subjective scale. The easiest way to deal with these subjective metrics is to provide supplier ratings, tariff types, green tariff labels, etc to enable filtering or options based on a consumer's comfort level.

The smart tariff, smart comparison library has the ambition to be used for not only the best price, but the best overall cost and carbon performance for the consumer's situation and time horizon. This means all of the outcomes should be acheieved within a finite time period, for example the payback of capital improvements and deliver energy when it is needed.

Some of the more detailed design decisions such as data sources and structures have been  taken on a practical basis. The library *tries* to abstract these so that as they change or if the models are extended, interfaces can remain the same while the internal implementation is free to change.

At the highest level the model provides an answer to which combination of "things" is "best". Best is defined as lowest cost. The initial combination of "things" is energy use and tariff considering the tariff, its product and supplier characteristics as user preferences.

[^1]: Cost can be viewed as lowest financial cost, lowest carbon impact or potentially money/CO2 ratio

# STSC Objects

The main objects describing smart tariff, smart comparison are modeled on the mathematical relations as well as considering the market structure of a supplier and product. Generally this model has been used for historical analysis of cost when looking at energy combined with tariff e.g. £ 0.10 / kWh * 20,000 kWh per year = £ 200 per year.

The Supplier and Product constructs allow for attributes to be placed on them such that filter criteria can drive the list of tariffs that are presented as options. Products also contain more cost/benefit/qualifying properties and have different tariffs depending on the grid service point (GSP) code. Products  often contain constraints and benefits like dual fuel contracts (electricity and gas), flexibility rewards (£40 cashback for use of battery), loyalty (renew at preferred rate), payment type discounts (direct debit discount), import/export linked pricing that will effect the overall cost.

A Tariff is the lowest level describing the unit rates and standing charges associated with a Product. This is where the structural types of flat, tou and dynamic are implemented.

Energy is the measurement from a meter in kWh that is produced at 30 minute resolution.

With the working definitions, the implementation is abstracted into a timeseries analysis where there are operations between timeseries, energy and tariff, yielding additional timeseries, energy-cost. The Product and Supplier objects through user preferences help the selection of tariffs.  

```mermaid
erDiagram
    SUPPLIER ||--|{ PRODUCT : offers
    PRODUCT ||--|{ TARIFF : prices
    ENERGY ||--|| ENERGY-COST : values
    TARIFF ||--|| ENERGY-COST : rates
    PRODUCT ||--|| TOTAL-COST : has
    ENERGY-COST |{--|| TOTAL-COST : item
```

[^2]: Object design with relationships shown for the domain of product selection based on total cost and supplier/product preferences

The overriding interesting aspect to switching energy provider is the promise of lower cost in the *future*. The structures take on a different meaning as they are cast forward in time. Specifically, Energy becomes Predicted Energy, Tariffs become Predicted Tariffs.

A naive approach to energy forecasting is using historical consumption directly, modeling the future based directly on the past. This is probably acceptable if the home energy system is stationary and the occupants are the same - and this is often the case. With an encouragement towards low carbon technology and energy efficiency, there is an opportunity to model changes to the future energy demand.

With tariffs, care needs to be taken to present a predicted tariff as much as possible. With fixed rate offers from suppliers this is done with some certainty. Both flat rate and TOU have certainty in the future, whereas a dynamic tariff will be floating with some potential contractual upper bounds. Historical prices paid are usually very interesting for consumers to see side by side with future planning, however if that rate is not available in the future it is not comparable. Only tariffs that can be obtained into the future are comparible, the consumers current contract may or may not exist in that set. 

Like unpredictable energy consumption, unpredictable dynamic tariffs could present themselves as to their historical performance. It is a big debate as to whether this is good enough, but is left to the consumer to decide knowing the risk in fluctuating prices.

Using the same object definitions they can be interpreted into future looking equivalents. It can also be seen that the pattern of cost calculation can also work for carbon using a sufficiently resolved carbon intensity value. However future carbon would suffer from the same issue of predicting carbon intensity values.

An approach, and some may argue more robust, would be to use statistics to help with misalignment precision of the future. For instance it is much harder to forecast a precise 30 minute carbon intensity value e.g. on the 10th of March 2030 at 10:30am the carbon intensity will be 0.909, versus for the month of March 2030 the afternoon carbon intensity will be 0.001 and the evening will be 2.01.

```mermaid
erDiagram
    SUPPLIER ||--|{ PRODUCT : offers
    PRODUCT ||--|{ PREDICTED-TARIFF : prices
    PREDICTED-ENERGY ||--|| PREDICTED-ENERGY-COST : values
    PREDICTED-TARIFF ||--|| PREDICTED-ENERGY-COST : rates
    PRODUCT ||--|| TOTAL-PREDICTED-COST : has
    PREDICTED-ENERGY-COST |{--|| TOTAL-PREDICTED-COST : item
    PREDICTED-ENERGY ||--|| PREDICTED-CARBON : consumes
    PREDICTED-CARBONINTENSITY ||--|| PREDICTED-CARBON : produces
```
[^3]: Modified entities showing forward looking predictions and a demonstration of model extension with carbon.

The overall system function relies heavily on the Products object. As a prototype it is useful, however after implementation and use, the opportunity to restructure should be taken to eliminate some rundancy in data structures and make some of the supporting objects more independent of a Products class.

However, it is important to show how this class is used in its current form. There are a number of examples how Energy, Tariff and Cost objects can be used independently as well as the Products at https://observablehq.com/collection/@joshuacooper/smart-tariff-smart-comparison

```mermaid
classDiagram
class Products{
		_token~string~
		filteredProducts~Array~
    filteredTariffs~Tariffs~
    gsp~string~
    predictedEnergy~Timeseries~
    products~Array~
    tariffs~Tariffs~
    suppliers~Suppliers~
    calcCosts()
    calcSmart()
    favourite(id)
    filter()
    load()
    loadgsp()
    loadtariffs()
    match(criteria)
    matchTariffs(criteria)
    setSuppliers(Suppliers)
    sortSmart()
    sortTariffs()
}
```

The first element to understand are the data sources. The Glow service is used to source energy, tariff, products and supplier data. There is a registration to the Glow service in order to gain permission to pull data from the DCC. The registration then has access to the grid service point (GSP) code that is required for loading tariffs.

```json
{
  id: 3,
  supplierid: 68,
  fkid: 'BANA-FIX-12M-20-09-22',
  displayname: 'Banana 12M Fixed',
  name: 'Banana 12M Fixed September 2020 v2',
  description: "This tariff features 100% renewable electricity and fixes your unit rates and standing charge for 12 months. There are no exit fees, so if you change your mind, you're in control.",
  is_variable: false,
  is_green: false,
  is_prepay: false,
  available_from: '2020-09-22T00:00:00+01:00',
  available_to: null,
  term: 12,
  brand: 'BANANA',
  type: 'flat'
}
```



```json
 {
   id: 0,
   supplierId: 68,
   supplierName: 'Banana',
   whiteLabelId: 0,
   dataAvailable: 'true',
   supplierRank: 1,
   overallRating: 4.65,
   complaintsRating: 4,
   complaintsNumber: 13.6,
   billsRating: 5,
   billsAccuracy: 99.9,
   contactRating: 5,
   contactTime: '00:15',
   contactEmail: '99.9',
   contactSocialMedia: '-1',
   switchRating: 5,
   switchPercent: 99.7,
   guaranteeRating: 5,
   guaranteesList: [Array],
   contactInformation: [Object],
   billingInformation: [Object],
   openingHours: [Object],
   fuelMix: [Object]
}
```



## Estimation

For cases where there is little data available, estimation of the annual energy consumption is necessary. 



The Use of Timeseries



Where does data come from? Glow and the DCC



Use Case 1 - Using Energy and Tariff

Use Case 2 - Selecting a Tariff from a number on offer

Use Case 3 - Calculating Carbon

Use Case 4 - Electrification of Gas Energy

Use Case 5 - Projected EV Costs

Use Case 6 - Analysis of carbon/cost ratio in tariff selection

Use Case 7 - Analysis of Export



Specific Tools - Runkit, ObservableHQ, NodeRed

Integration



