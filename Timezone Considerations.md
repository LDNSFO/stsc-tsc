Timezone Considerations

Summary

Our calculation of costs in POC4 pertain to annual and monthly cost for comparison purposes. 

Consumption is summarised as a monthly value on a chart as well as a time of use showing daily patterns of use. The daily display is organised as a block 0-47 representing a 30 minute period of the day. The blocks start at 00:00 hours and end at 23:30 for the day. The average consumption for the   

#### Assertion 1

For the calculation of costs, it is only necessary to make sure the unit rates align with the consumption for the time block, there is no need to "change" timeszones on a seasonal basis or reflect a particular locale. Succinctly, there are two time streams to coordinate, that of the consumption and that of the unit rate - this will result in a cost timeseries that should be continuous in time with no gaps.

##### Background

Within metering the meter records consumption in a continuous way, filling a register that until an elapsed real time moves to the next time register. 

- flat rates have no time alignment issues as they apply in all time,
- dynamic rates would necessarily be specified with the correct corresponding time slot, and
- time of use tariffs are up to the publisher of the tariff to specify and/or technical systems that store the time of use rates

TOU Case Study

Octopus has several time of use tariffs on the market with different rates and effective times. They generally have Go and Go Faster and publish the rates and applicable times on a daily basis.

Taking a detailed look at Go as published by the tariff APIs from Octopus

```html
https://api.octopus.energy/v1/products/GO-18-06-12/electricity-tariffs/E-1R-GO-18-06-12-A/standard-unit-rates/
```

The standard unit rates are all described as UTC with a Z indicating the timezone that is reflective of the time stamp for the start and end of a rate period. When British Summer Time (BST) comes in to effect, the time that is published changes, leaving nothing open to interpretation. 

```json
{
value_exc_vat: 4.76,
value_inc_vat: 4.998,
valid_from: "2021-03-29T23:30:00Z",
valid_to: "2021-03-30T03:30:00Z"
},
{
value_exc_vat: 13.45,
value_inc_vat: 14.1225,
valid_from: "2021-03-29T03:30:00Z",
valid_to: "2021-03-29T23:30:00Z"
},
{
value_exc_vat: 4.76,
value_inc_vat: 4.998,
valid_from: "2021-03-28T23:30:00Z",
valid_to: "2021-03-29T03:30:00Z"
},
{
value_exc_vat: 13.45,
value_inc_vat: 14.1225,
valid_from: "2021-03-28T03:30:00Z",
valid_to: "2021-03-28T23:30:00Z"
},
{
value_exc_vat: 4.76,
value_inc_vat: 4.998,
valid_from: "2021-03-28T00:30:00Z",
valid_to: "2021-03-28T03:30:00Z"
},
{
value_exc_vat: 13.45,
value_inc_vat: 14.1225,
valid_from: "2021-03-27T04:30:00Z",
valid_to: "2021-03-28T00:30:00Z"
},
{
value_exc_vat: 4.76,
value_inc_vat: 4.998,
valid_from: "2021-03-27T00:30:00Z",
valid_to: "2021-03-27T04:30:00Z"
},
{
value_exc_vat: 13.45,
value_inc_vat: 14.1225,
valid_from: "2021-03-26T04:30:00Z",
valid_to: "2021-03-27T00:30:00Z"
},
```

It is not a very concise statement of the tariff, but essentially the API is delivering the marketing promise:

​	*The smart tariff with **super cheap electricity for 5p/kWh between 00:30 - 04:30 every night*** 

When BST is in effect, the start time of the peak tariff is 4:30am local time or 03:30 UTC and when GMT is in effect 4:30am or 04:30 UTC. Octopus is consistently publishing the time to correspond to the marketing and sales description.

What occurs on the night of GMT to BST is that:

|Time index|Time|Local time (en-GB) |Rate|
|----------------|-------|-----------|-----------|
|46|2021-03-27T23:00:00Z||14.1225|
|47|2021-03-27T23:30:00Z||14.1225|
|0|2021-03-28T00:00:00Z||14.1225|
|1|2021-03-28T00:30:00Z||4.998|
|2|2021-03-28T01:00:00Z||4.998|
|3|2021-03-28T01:30:00Z||4.998|
|4|2021-03-28T02:00:00Z||4.998|
|5|2021-03-28T02:30:00Z||4.998|
|6|2021-03-28T03:00:00Z||4.998|
|7|2021-03-28T03:30:00Z||14.1225|
|8|2021-03-28T04:00:00Z||14.1225|
|9|2021-03-28T04:30:00Z||14.1225|
|...|...|...|...|
|46|2021-03-28T23:00:00Z||14.1225|
|47|2021-03-28T23:30:00Z||4.998|
|0|2021-03-29T00:00:00Z||4.998|
|1|2021-03-29T00:30:00Z||4.998|
|2|2021-03-29T01:00:00Z||4.998|

The slot index changes too, which isn't a surprise. However within the 

TOU on Meter

TOU within STSC

The STSC time of use data structure is stored with a timezone

### Time versus Local Time

Time refers to real physical time, and for our purposes stored and displayed as Coordinated Univeral Time (UTC). UTC does not change time for things like daylight savings and stays the same no matter where you are on the earth. Although UTC is guided by counting oscillations at an atomic level, it is an artificially constructed system of time keeping striving to synchronise with the Earth's cycles of movement. UTC is subject to adjustment and error. The main point is that it is agreed, coordinated and good enough for practical purposes.  

Local time is how we locally agree, within a geo-political boundary to communicate time. This is subject to timezone offsets that in many places change throughout the year. Local time is a lense in which to view time as described above. It is important because we orient our lives (when we wake, when we sleep) in local time.

Local time is defined by ISO timezones and is commonly accounted for in computer technology with other local settings (deisgnated as *locale*) such as language, date time formatting conventions and currency. The computer's user interface software and applications supporting by the computers operating system can decide how they use locale to provide an acceptable user experience. For instance your mobile phone typically changes the time displayed on the phone to local time.

Our approach is to work in UTC time for all calculations related to cost, putting a requirement on unit rates to be explicit for the UTC time period.  

Ironically when discussing solar energy there is almost a direct view on to the Earth's cycles with solar maximum being dictated by position on the Earth. As a side note, our next leap second could occur 30 June 2021, but the Earth as of late is speeding up its rotations, so the committee that decides these things can take a view on whether or not to add a leap second. 

Aggregation and Local Time



## Monthly Cost

Monthly Costs are calculated by using the Luxon Javascript library for calculating the time offsets for 

```javascript
let durMonth = Duration.fromISO('P1M');
let startThisMonth = DateTime.utc().startOf('month');
```

Annual Cost

For the purposes of our library, annual costs are calculated as the last full 12 months







Assertion 2

Aggregation of any timeseries cost, energy or tariff should not under or over allocate values in time. 

##### Assertion 2 

If displaying data to an end user it could be important to reflect their view of time. This is important on day views where 48 x 30 minute timeslots should correspond with the viewer's understanding of time. For an end user, the day  

Assertion 3

If projecting forward a consideration is just time alignment, although different days may result in different outcomes.

Assertion 4



Using the aggregation with Duration offsets provided by luxon and the ISO designators, there is an issue. For instance 
```
    let params = {
        from: '2019-08-10T01:00:00.000+01:00',
        to: '2021-04-01T00:30:00.000+01:00',
        interval: 'P1M',
        aggregate: 0
      }
      
    myenergy.consumption(params) yields ->
    
      Timeseries {
        start: 1565395200000,
        end: 1612051200000,
        interval: 2592000000,
        _modified: 1617524457113,
        _rawdata: Float64Array(19) [
               0,      0, 397650,
          549890, 606332, 534674,
          557006, 631002, 698722,
          560796, 621892, 597531,
          550066, 511529, 540298,
          556533, 658320, 565191,
          594708
        ],
        _rawinterval: 0,
        _fetchtime: 1617524457105,
        _type: 0
      }
      
    energy.byMonth yields ->
      [
        [ 1585699200000, 683979 ],
        [ 1588291200000, 614257 ],
        [ 1590969600000, 616055 ],
        [ 1593561600000, 621205 ],
        [ 1596240000000, 561564 ],
        [ 1598918400000, 525922 ],
        [ 1601510400000, 550838 ],
        [ 1604188800000, 555873 ],
        [ 1606780800000, 675934 ],
        [ 1609459200000, 582980 ],
        [ 1612137600000, 562096 ],
        [ 1614556800000, 598580 ]
      ]
```



# Excel Calculations

Excel deals with things as UTC, for a format of Date (yyyy-mm-ddThh:MM) and a * with the time format seems not to make a difference.

```
28/03/2020	2020-03-28T23:30	23:30:00	1585438200000	144
29/03/2020	2020-03-29T00:00	00:00:00	1585440000000	432
29/03/2020	2020-03-29T00:30	00:30:00	1585441800000	215
29/03/2020	2020-03-29T01:00	01:00:00	1585443600000	143
29/03/2020	2020-03-29T01:30	01:30:00	1585445400000	122
29/03/2020	2020-03-29T02:00	02:00:00	1585447200000	89
```
With an Excel pivot table, the summary that is calculated is compared to a summary calculated with the byMonth method in the Timeseries  class, not all data is returned in the byMonth method, only the last 12 months, therefore the number of comparisons is reduced.

|Time Period|Sum of Wh|Timeseries->byMonth|
|---:|----:|-----:|
|**2020**| **6,579,039** ||
|Feb|538,727||
|Mar|634,685||
|Apr|683,979|683,979|
|May|614,257|614,257|
|Jun|616,055|616,055|
|Jul|621,205|621,205|
|Aug|561,564|561,564|
|Sep|525,922|525,922|
|Oct|550,838|550,838|
|Nov|555,873|555,873|
|Dec|675,934|675,934|
|**2021**|**1,145,076**|
|Jan|582,980|582,980|
|Feb|562,096|562,096|
|**Grand Total**|7,724,115||



## Add TOU and HH with Locale features

If we could use the locale feature to understand how many regions of timezone changes we would need throughout the year? 

The current half hour code works on the basis of consecutive slots. To change, use the DateTime with/without UTC to put value into slot.

```javascript
/**
 * Current code
 * /
 
        let startSlot = DateTime.fromMillis(this.start).hour * 2;
        
        if(DateTime.fromMillis(this.start).minute > 29) {
            startSlot++;
        };

        let slot = startSlot;

        this._rawdata.forEach((value) => {
            workingData[parseInt(slot, 10)].push(value);
            if(slot < 47) {
                slot++;
            } else {
                slot = 0;
            }
        });
```

Modified for half hours

```javascript
/**
 * Change the interator to work from the epoch time stamp
 * /

let indexTS = this.start;

let slot = DateTime.fromMillis(indexTS).hour * 2;
        
if(DateTime.fromMillis(indexTS).minute > 29) {
   slot++;
};

this._rawdata.forEach((value) => {
   workingData[parseInt(slot, 10)].push(value);
   slot = DateTime.fromMillis(indexTS).hour * 2;
        
   if(DateTime.fromMillis(indexTS).minute > 29) {
      slot++;
   };
   // 30 minutes fixed as it is half hour
   indexTS += 1800000;
});
```