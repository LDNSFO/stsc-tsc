export default class Carbon extends mTimeseries.default {
    _token: string;
    energy: mEnergy.default;
    carbonintensity: mCarbonIntensity.default;
    calc(): void;
}
import * as mTimeseries from "./timeseries.js";
import * as mEnergy from "./energy.js";
import * as mCarbonIntensity from "./carbonintensity.js";
