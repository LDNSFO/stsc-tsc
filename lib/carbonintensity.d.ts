export default class CarbonIntensity extends mTimeseries.default {
    _token: string;
    gsp: string;
    _intervalDecode(intervalCode: any): number | undefined;
    load(): Promise<void>;
}
import * as mTimeseries from "./timeseries.js";
