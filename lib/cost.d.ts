export default class Cost extends mTimeseries.default {
    energy: mEnergy.default;
    tariff: mTariff.default;
    /**
     * pence per W to pounds per kW (or any other decimel money)
     */
    _units_multiplier: number;
    _monetary_multiplier: number;
    _vat: number;
    _annual: number;
    _byMonth: any[];
    /**
     * Rollup costs as annual, use the byMonth method
     * Note: indexing is last 12 months, not Jan - Dec
     */
    get annual(): number;
    /**
     * Total cost for the time period specified
     * includes the standing charge and the
     * consumption based charges
     */
    get total(): number;
    /**
     * Facade function
     * Consumption based charges, calculated differently for each
     * tariff type
     */
    get consumption(): number;
    /**
     * Facade function for standing charge calculation
     */
    get standingCharge(): {
        cost: number;
        days: number;
    };
    /**
     * Initialise the cost object with energy and tariff
     * objects.
     * @param {*} energy
     * @param {*} tariff
     */
    init(data: any): void;
    /**
     * Private consumption calculation that can be used to break up time ranges and apply summary functions
     *
     * @param {*} params
     */
    _consumption(params: any): number;
    /**
     * Private function for calculating the standing charges based on number of days
     * @param {*} params
     */
    _standingCharge(params: any): {
        cost: number;
        days: number;
    };
}
import * as mTimeseries from "./timeseries.js";
import * as mEnergy from "./energy.js";
import * as mTariff from "./tariff.js";
