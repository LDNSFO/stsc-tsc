/**
 * Energy
 *
 * This is the main energy class that is used to pull 30 minute energy data
 * there are summarising methods that are made for energy analysis
 * plus the inherited methods of the timeseries as general purpose data manipulation
 */
export default class Energy extends mTimeseries.default {
    /**
     * type is to identify generation, consumption, import, export, submeter
     * fuel can be used to match to tariff (gas, electricity)
     * direction is can be generation, consumption, import, export
     */
    type: string;
    fuel: string;
    direction: string;
    /**
     * Allows the object to be named
     */
    name: string;
    _token: string;
    _cachedProfile: {
        nonzeromin: number;
        min: number;
        max: number;
        mean: number;
        stddev: number;
        histogram: any[];
        weekly: {
            averageDaily: number;
            varianceDaily: number;
            averageBaseline: number;
            varianceBaseline: number;
            averageWeekDay: number;
            varianceWeekDay: number;
            averageWeekEndDay: number;
            varianceWeekEndDay: number;
            averageDayUse: number;
            varianceDayUse: number;
            averageEveningUse: number;
            varianceEveningUse: number;
            averageOffPeakUse: number;
            varianceOffPeakUse: number;
        }[];
    } | null;
    _intervalDecode(intervalCode: any): number | undefined;
    /**
     * With the supply of a valid token, load will fetch
     * binary energy data from the server to be used with the
     * other classes or standalone for running aggregations
     * and analysis
     */
    load(): Promise<void>;
    /**
     * Read a binary stsc object from memory
     *
     */
    _fromBinary(binary: any): void;
    /**
     * Profile summarises the energy data into the main statistics
     *
     * For ease of use as an object, a histogram and weekly summary is
     * attached to the object
     */
    profile(): {
        nonzeromin?: number;
        min?: number;
        max?: number;
        mean?: number;
        stddev?: number;
        histogram?: any[];
        weekly?: {
            averageDaily: number;
            varianceDaily: number;
            averageBaseline: number;
            varianceBaseline: number;
            averageWeekDay: number;
            varianceWeekDay: number;
            averageWeekEndDay: number;
            varianceWeekEndDay: number;
            averageDayUse: number;
            varianceDayUse: number;
            averageEveningUse: number;
            varianceEveningUse: number;
            averageOffPeakUse: number;
            varianceOffPeakUse: number;
        }[];
    };
    /**
     * Used to extract aggregates from the Energy data
     * there are implementations of some calendar summaries as
     * Luxon does not correctly compute things like P1M on
     * the monthly boundaries
     *
     * @param from from ISO formatted date time to start
     * @param to ISO formatted date time to stop
     * @param intervalISO ISO interval value for the aggregate to be returned
     * @param aggregate Timeseries aggregate function
     * @param calendar use a fixed calendar to summarise
     * @returns Timeseries with aggregate values
     */
    consumption(params: any): any;
    /**
     * Adds energy to the half hour slot specified
     * this can be used for EV additional load
     * @param params
     */
    addEnergy(params: any): Energy;
    /**
     * While preserving the overall energy, move an amount of energy from one
     * half hour to another. This is based on percentage in order to not
     * go negative, although it could be made more sophisticated
     * TBD: make this shiftEnergyPercent, create new shiftEnergy with absolute values
     * @param {*} params
     */
    shiftEnergy(params: any): Energy;
    /**
     * Big, maybe too big, analysis of behavioural parameters on a week by week basis
     * mostly used in profile
     * TBD: check if there are any issues around localtime
     * @param {*} params
     */
    byWeek(params: any): {
        averageDaily: number;
        varianceDaily: number;
        averageBaseline: number;
        varianceBaseline: number;
        averageWeekDay: number;
        varianceWeekDay: number;
        averageWeekEndDay: number;
        varianceWeekEndDay: number;
        averageDayUse: number;
        varianceDayUse: number;
        averageEveningUse: number;
        varianceEveningUse: number;
        averageOffPeakUse: number;
        varianceOffPeakUse: number;
    }[];
    _extractSlot(params: any, list: any): number;
}
import * as mTimeseries from "./timeseries";
