"use strict";
/**
 * stsc-tsc v0.4.x | index.ts
 *
 * Copyright 2020 - 2021 Hildebrand Technology Limit
 * Author: Joshua Cooper - jcooper@hildebrand.co.uk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy o
 * this software and associated documentation files (the "Software"), to deal in th
 * Software without restriction, including without limitation the rights to use
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copie
 * of the Software, and to permit persons to whom the Software is furnishe
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.Carbon = exports.CarbonIntensity = exports.Cost = exports.Suppliers = exports.Products = exports.Tariff = exports.Energy = exports.Timeseries = exports.STSC = void 0;
/**
 * General export of all of the classes
 * it is allowed to mix Javascript and Typescript
 */
/**
 * Typescript files
 */
var stsc_1 = require("./stsc");
Object.defineProperty(exports, "STSC", { enumerable: true, get: function () { return stsc_1.default; } });
/**
 * Javascript files
 */
var timeseries_js_1 = require("./timeseries.js");
Object.defineProperty(exports, "Timeseries", { enumerable: true, get: function () { return timeseries_js_1.default; } });
var energy_js_1 = require("./energy.js");
Object.defineProperty(exports, "Energy", { enumerable: true, get: function () { return energy_js_1.default; } });
var tariff_js_1 = require("./tariff.js");
Object.defineProperty(exports, "Tariff", { enumerable: true, get: function () { return tariff_js_1.default; } });
var products_js_1 = require("./products.js");
Object.defineProperty(exports, "Products", { enumerable: true, get: function () { return products_js_1.default; } });
var suppliers_js_1 = require("./suppliers.js");
Object.defineProperty(exports, "Suppliers", { enumerable: true, get: function () { return suppliers_js_1.default; } });
var cost_js_1 = require("./cost.js");
Object.defineProperty(exports, "Cost", { enumerable: true, get: function () { return cost_js_1.default; } });
var carbonintensity_js_1 = require("./carbonintensity.js");
Object.defineProperty(exports, "CarbonIntensity", { enumerable: true, get: function () { return carbonintensity_js_1.default; } });
var carbon_js_1 = require("./carbon.js");
Object.defineProperty(exports, "Carbon", { enumerable: true, get: function () { return carbon_js_1.default; } });
