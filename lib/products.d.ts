export default class Products {
    _baseURL: string;
    gsp: string;
    token: string;
    products: any[];
    tariffs: any[];
    predictedEnergy: mEnergy.default;
    filteredProducts: any[];
    filteredTariffs: any[];
    suppliers: any[];
    rebate: {
        amount: number;
        period: number;
    };
    /**
     * Load the products from the server
     */
    load(): Promise<void>;
    _modified: any;
    loadgsp(): Promise<void>;
    _performOperationsForTariffLoad(tariff: any): Promise<mTariff.default>;
    loadtariffs(): Promise<void>;
    setSuppliers(suppliers: any): void;
    calcCosts(): void;
    calcSmart(): void;
    sortTariffs(order?: string): void;
    sortProducts(order?: string): void;
    sortSmart(order?: string): void;
    match(params: any): void;
    filter(params: any): void;
    matchTariffs(params: any): void;
    favourite(id: any): void;
}
import * as mEnergy from "./energy";
import * as mTariff from "./tariff";
