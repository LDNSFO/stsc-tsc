"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
//import { Cost } from './cost';
// import { Tariff } from './tariff';
const mCost = require("./cost.js");
const mTariff = require("./tariff.js");
const mEnergy = require("./energy.js");
const Cost = mCost.default || mCost;
const Tariff = mTariff.default || mTariff;
const Energy = mEnergy.default || mEnergy;
class Products {
    constructor() {
        // In future only load based on GSP and/or features
        this._baseURL = 'https://beta.smarttariffsmartcomparison.org/';
        this.gsp = '';
        this.token = '';
        this.products = [];
        this.tariffs = [];
        this.predictedEnergy = new Energy();
        this.filteredProducts = [];
        this.filteredTariffs = [];
        this.suppliers = [];
        this.rebate = { amount: 0, period: 12 };
        // this.suppliers.load().then();
    }
    /**
     * Load the products from the server
     */
    load() {
        return __awaiter(this, void 0, void 0, function* () {
            let stscProductsURL = this._baseURL + '/products.php';
            yield axios_1.default(stscProductsURL, {
                headers: {
                    'token': this.token
                }
            }).then((response) => {
                if (response.status === 200) {
                    this._modified = response.headers['last-modified'];
                    this.products = response.data;
                }
            })
                .catch((error) => {
                throw new Error(error);
            });
        });
    }
    loadgsp() {
        return __awaiter(this, void 0, void 0, function* () {
            let stscServiceURLGSP = "https://beta.smarttariffsmartcomparison.org/gsp.php";
            // let localGsp = localStorage.getItem('gsp');
            let localGsp = false;
            if (!localGsp) {
                yield axios_1.default(stscServiceURLGSP, {
                    headers: {
                        'Cache-Control': 'no-store',
                        'token': this._token
                    }
                }).then((response) => {
                    if (response.status === 200) {
                        this._modified = response.headers['last-modified'];
                        this.gsp = response.data.gsp;
                    }
                })
                    .catch((error) => {
                    throw new Error(error);
                });
            }
        });
    }
    _performOperationsForTariffLoad(tariff) {
        return __awaiter(this, void 0, void 0, function* () {
            tariff.gsp = this.gsp;
            // let product = this.products.find((element) => element.fkid === tariff.fkid);
            let productIndex = this.products.findIndex((element) => element.fkid === tariff.fkid);
            let product = this.products[productIndex];
            tariff.brand_image = '/assets/' + product.brand.toLowerCase() + '.svg';
            let supplierinfo = this.suppliers.find((element) => element.supplierId === product.supplierid);
            tariff.brand_score = Math.round(supplierinfo.overallRating);
            if (product.is_green)
                tariff.green = true;
            if (product.is_variable)
                tariff.is_variable = true;
            if (product.is_prepay)
                tariff.is_prepay = true;
            if (!product.is_prepay)
                tariff.is_smart = true;
            tariff.term = product.term;
            if (product.type === 'tou')
                tariff.is_ev = true;
            if (product.type === 'dynamic')
                tariff.is_ev = true;
            if (product.type === 'tou')
                tariff.is_solar = true;
            if (product.type === 'dynamic')
                tariff.is_solar = true;
            if (product.type === 'tou')
                tariff.is_heatpump = true;
            if (product.type === 'dynamic')
                tariff.is_heatpump = true;
            // this has been made async/await in order to load dynamic tariffs
            // that have to come from the server as an additional call
            let objTariff = new Tariff();
            yield objTariff.setTariff(tariff);
            this.products[productIndex].tariff = objTariff;
            return objTariff;
        });
    }
    loadtariffs() {
        return __awaiter(this, void 0, void 0, function* () {
            // web call to load tariffs
            // then load into structure
            // if(!localGsp) localGsp = "J";
            let servertariffs = [];
            const tariffLoadResponses = [];
            let stscServiceURL = "https://beta.smarttariffsmartcomparison.org/tariff.php?gsp=" + this.gsp;
            yield axios_1.default(stscServiceURL, {
                headers: {
                //'Cache-Control': 'no-store'
                }
            }).then((response) => __awaiter(this, void 0, void 0, function* () {
                if (response.status === 200) {
                    servertariffs = response.data;
                    const lastTariffLoadResponse = yield servertariffs.reduce((accumulatorPromise, tariff) => {
                        return accumulatorPromise
                            .then((result) => {
                            if (result)
                                tariffLoadResponses.push(result);
                            return this._performOperationsForTariffLoad(tariff);
                        })
                            .catch(err => {
                            if (err) {
                                tariffLoadResponses.push(err);
                            }
                            return this._performOperationsForTariffLoad(tariff);
                        });
                    }, Promise.resolve());
                    if (lastTariffLoadResponse)
                        tariffLoadResponses.push(lastTariffLoadResponse);
                    this.tariffs = tariffLoadResponses;
                }
            }))
                .catch((error) => {
                throw new Error(error);
            });
        });
    }
    setSuppliers(suppliers) {
        this.suppliers = suppliers.suppliers;
    }
    calcCosts() {
        // this.tariffs.forEach((tariff) => {
        // let product = this.products.find((element) => element.fkid === tariff.id);
        // tariff.brand_image = '/assets/' + product.brand.toLowerCase() + '.svg';
        // let supplierinfo = this.suppliers.find((element) => element.supplierId === product.supplierid);
        // tariff.brand_score = Math.round(supplierinfo.overallRating);
        // should we attach cost to the tariff or products object to be 
        // able to grab it, summarise it etc
        // let cost = new Cost();
        // console.log(tariff);
        // console.log(this.predictedEnergy);
        // cost.init({ energy: this.predictedEnergy, tariff: tariff });
        // console.log(cost.annualcost);
        // console.log(cost.byMonth());
        // tariff.cost = cost;
        // });
        // guard for backwards compatibility
        for (let i = 0; i < this.products.length; i++) {
            let product = this.products[i];
            if (product.tariff !== undefined) {
                let cost = new Cost();
                cost.init({ energy: this.predictedEnergy, tariff: product.tariff });
                product.annualcost = +cost.annual.toFixed(2);
            }
        }
        // this.products.forEach((product) => {    
        //     // should we attach cost to the tariff or products object to be 
        //     // able to grab it, summarise it etc
        //     if(product.tariff !== undefined) {
        //         let cost = new Cost();
        //         cost.init = { energy: {...this.predictedEnergy}, tariff: {...product.tariff } };
        //         product.annualcost = cost.annual;
        //     }
        // });
    }
    calcSmart() {
        // get this.preferences -- need to set from frontend
        // then score based on those
        // right now just set to zero
        this.tariffs.forEach((tariff) => {
            tariff.score = Math.round(Math.random(40) * 40);
        });
        // right now just set to zero
        this.products.forEach((product) => {
            product.smart_score = Math.round(Math.random(40) * 40);
        });
    }
    sortTariffs(order = 'asc') {
        if (order === 'asc') {
            this.tariffs = this.tariffs.sort((a, b) => {
                return a.annualcost - b.annualcost;
            });
        }
        else {
            this.tariffs = this.tariffs.sort((a, b) => {
                return b.annualcost - a.annualcost;
            });
        }
    }
    sortProducts(order = 'asc') {
        if (order === 'asc') {
            this.products = this.products.sort((a, b) => {
                return a.annualcost - b.annualcost;
            });
        }
        else {
            this.products = this.products.sort((a, b) => {
                return b.annualcost - a.annualcost;
            });
        }
    }
    sortSmart(order = 'des') {
        if (order === 'asc') {
            this.tariffs = this.tariffs.sort((a, b) => {
                return a.score - b.score;
            });
        }
        else {
            this.tariffs = this.tariffs.sort((a, b) => {
                return b.score - a.score;
            });
        }
    }
    match(params) {
        // return all that match any one criteria OR
        // params { supplierating, type, green, prepay, term, variable }
        const result = [];
        const map = new Map();
        this.filteredProducts = [];
        this.products.forEach((product) => {
            params.forEach((criteria) => {
                if (criteria[Object.keys(criteria)] === product[Object.keys(criteria)]) {
                    result.push(product);
                }
            });
        });
        // reduce to unqiue id
        for (const item of result) {
            if (!map.has(item.id)) {
                map.set(item.id, true); // set any value to Map
                this.filteredProducts.push(item);
            }
        }
    }
    filter(params) {
        // return all that match any one criteria OR
        // return only that match all criteria AND
        // this.filteredProducts = this.products.filter(product => {
        //     keys.forEach((key) => {
        //         console.log(product[key]);
        //     })
        //     return product.is_green == params.is_green;
        // }) 
    }
    matchTariffs(params) {
        // return all that match any one criteria OR
        // params { supplierating, type, green, prepay, term, variable }
        const result = [];
        const map = new Map();
        this.filteredTariffs = [];
        this.tariffs.forEach((tariff) => {
            params.forEach((criteria) => {
                if (criteria[Object.keys(criteria)] === tariff[Object.keys(criteria)]) {
                    result.push(tariff);
                }
            });
        });
        // reduce to unqiue id
        for (const item of result) {
            if (!map.has(item.id)) {
                map.set(item.id, true); // set any value to Map
                this.filteredTariffs.push(item);
            }
        }
    }
    favourite(id) {
        // this is for the future
    }
}
exports.default = Products;
