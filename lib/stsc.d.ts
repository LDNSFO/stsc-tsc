/**
 * STSC
 * Entry class that makes working with server objects personalised
 *
 * Copyright 2020 - 2021 Hildebrand Technology Limit
 * Author: Joshua Cooper - jcooper@hildebrand.co.uk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy o
 * this software and associated documentation files (the "Software"), to deal in th
 * Software without restriction, including without limitation the rights to use
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copie
 * of the Software, and to permit persons to whom the Software is furnishe
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import { Energy } from '.';
/**
 * Username and password strings passed to login method
 * @interface LoginOptions
 * @desc Username and password strings passed to login method
 */
export interface LoginOptions {
    username: string;
    password: string;
}
export default class STSC {
    token: string;
    private _baseURL;
    private VERSION;
    private _modified;
    gsp: string;
    epc: any;
    suppliers: any;
    products: any;
    predictedEnergy: any;
    constructor();
    /**
     * Provides a token from the main Glow service for access to
     * meter data, tariffs, etc
     *
     * Token is saved in token property of this object
     *
     * See Glowmarkt APIs for full explaination, however only a
     * this one call is needed - or you can get the token directly
     * from calling the Glow platform login or cut and paste from user
     *
     * @param param LoginOptions
     *
     */
    login(param: LoginOptions): Promise<void>;
    /**
     * Returns the estimated time that it will take to load
     * data from all sources, can take a while given
     * the nature of the remote data sources
     *
     * token must be set or there will be an error back from
     * the API
     *
     * @returns JSON string of loadtime parameters from server
     *
     */
    loadtime(): Promise<void>;
    /**
     * destroys server side data and flushes cache
     * token becomes unusable
     */
    logout(): Promise<void>;
    /**
     *
     * server side refresh of any data held in server cache
     * this is good as a failsafe in case there was a delay
     * in data construction that resulted in an incomplete cache
     */
    flushdata(): Promise<void>;
    /**
     * From the token, give back the GSP code that can be used for other queries
     */
    loadgsp(): Promise<void>;
    /**
     * Load EPC for the energy based on the assumption that the MPAN is being used
     * and the server knows which EPC to retun based on the login token
     * @param {*} params
     */
    loadepc(): Promise<void>;
    /**
     * Set suppliers that are in context, usually called before a load
     */
    setSuppliers(suppliers: any): void;
    /**
     * Set products that are in context, usually called after a load of the products
     */
    setProducts(products: any): void;
    /**
     * Set predictedEnergy
     */
    setPredictedEnergy(predictedEnergy: Energy): void;
    /**
     * Clears the token
     */
    clearToken(): void;
}
