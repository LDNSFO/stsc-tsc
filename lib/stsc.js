"use strict";
/**
 * STSC
 * Entry class that makes working with server objects personalised
 *
 * Copyright 2020 - 2021 Hildebrand Technology Limit
 * Author: Joshua Cooper - jcooper@hildebrand.co.uk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy o
 * this software and associated documentation files (the "Software"), to deal in th
 * Software without restriction, including without limitation the rights to use
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copie
 * of the Software, and to permit persons to whom the Software is furnishe
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const _1 = require(".");
class STSC {
    constructor() {
        this._modified = 0;
        this.gsp = '';
        this.epc = {};
        this.suppliers = [];
        this.products = [];
        this.predictedEnergy = new _1.Energy();
        this.token = '';
        // the base URL for the Smart Tariff application
        this._baseURL = 'https://beta.smarttariffsmartcomparison.org/';
        this.VERSION = '1.0.9';
    }
    /**
     * Provides a token from the main Glow service for access to
     * meter data, tariffs, etc
     *
     * Token is saved in token property of this object
     *
     * See Glowmarkt APIs for full explaination, however only a
     * this one call is needed - or you can get the token directly
     * from calling the Glow platform login or cut and paste from user
     *
     * @param param LoginOptions
     *
     */
    login(param) {
        return __awaiter(this, void 0, void 0, function* () {
            // authentication is from the Glowmarkt systems that are APIs
            // seperate from the smart tariff application
            const stscServiceURL = 'https://api.glowmarkt.com/api/v0-1/auth/';
            yield axios_1.default
                .post(stscServiceURL, {
                username: param.username,
                password: param.password,
            }, {
                headers: {
                    // the smart tariff application Id as it is known to Glowmarkt
                    applicationId: '6dc7e58b-4f10-4897-9a6b-a0971bee1235',
                },
            })
                .then((response) => {
                if (response.status === 200) {
                    this.token = response.data.token;
                    return response.data;
                }
            })
                .catch((err) => {
                // console.log(err);
            });
        });
    }
    /**
     * Returns the estimated time that it will take to load
     * data from all sources, can take a while given
     * the nature of the remote data sources
     *
     * token must be set or there will be an error back from
     * the API
     *
     * @returns JSON string of loadtime parameters from server
     *
     */
    loadtime() {
        return __awaiter(this, void 0, void 0, function* () {
            const stscServiceURL = this._baseURL + 'loadtime.php';
            yield axios_1.default(stscServiceURL, {
                headers: {
                    'Cache-Control': 'no-store',
                    token: this.token,
                },
            }).then((response) => {
                if (response.status === 200) {
                    this.loadtime = response.data;
                    return response.data;
                }
            });
        });
    }
    /**
     * destroys server side data and flushes cache
     * token becomes unusable
     */
    logout() {
        return __awaiter(this, void 0, void 0, function* () {
            const stscServiceURL = this._baseURL + 'delete.php';
            yield axios_1.default(stscServiceURL, {
                headers: {
                    'Cache-Control': 'no-store',
                    token: this.token,
                },
            }).then((response) => {
                if (response.status === 200) {
                    this.clearToken();
                    return response.data;
                }
            });
        });
    }
    /**
     *
     * server side refresh of any data held in server cache
     * this is good as a failsafe in case there was a delay
     * in data construction that resulted in an incomplete cache
     */
    flushdata() {
        return __awaiter(this, void 0, void 0, function* () {
            const stscServiceURL = this._baseURL + 'flush.php';
            yield axios_1.default(stscServiceURL, {
                headers: {
                    'Cache-Control': 'no-store',
                    token: this.token,
                },
            }).then((response) => {
                if (response.status === 200) {
                    return response.data;
                }
            });
        });
    }
    /**
     * From the token, give back the GSP code that can be used for other queries
     */
    loadgsp() {
        return __awaiter(this, void 0, void 0, function* () {
            const stscServiceURLGSP = this._baseURL + '/gsp.php';
            yield axios_1.default(stscServiceURLGSP, {
                headers: {
                    'Cache-Control': 'no-store',
                    token: this.token,
                },
            })
                .then((response) => {
                if (response.status === 200) {
                    this.gsp = response.data.gsp;
                }
            })
                .catch((error) => {
                throw new Error(error);
            });
        });
    }
    /**
     * Load EPC for the energy based on the assumption that the MPAN is being used
     * and the server knows which EPC to retun based on the login token
     * @param {*} params
     */
    loadepc() {
        return __awaiter(this, void 0, void 0, function* () {
            // token will match the address
            const stscServiceURL = 'https://beta.smarttariffsmartcomparison.org/epc.php';
            yield axios_1.default(stscServiceURL, {
                responseType: 'arraybuffer',
                headers: {
                    token: this.token,
                },
            })
                .then((response) => {
                if (response.status === 200) {
                    this.epc = response.data;
                }
            })
                .catch((error) => {
                throw error;
            });
        });
    }
    /**
     * Set suppliers that are in context, usually called before a load
     */
    setSuppliers(suppliers) {
        this.suppliers = suppliers;
    }
    /**
     * Set products that are in context, usually called after a load of the products
     */
    setProducts(products) {
        this.products = products;
    }
    /**
     * Set predictedEnergy
     */
    setPredictedEnergy(predictedEnergy) {
        this.predictedEnergy = predictedEnergy;
    }
    /**
     * Clears the token
     */
    clearToken() {
        this.token = '';
    }
}
exports.default = STSC;
