export default class Suppliers {
    _baseURL: string;
    suppliers: any[];
    token: string;
    /**
     * Load the suppliers from the server
     */
    load(): Promise<void>;
    _modified: any;
}
