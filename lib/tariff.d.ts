/**
 * Tariff class represents the type, unit rates and some characteristics that are specific
 * to region (GSP code). Contract length, exit fees are captured at this level as well, although
 * some suppliers may model these as Product attributes. It is handy to maintain them here for
 * calculations beyond simple filtering.
 *
 * The point of this class is to store and retrieve data structures so that higher level classes
 * can perform calculations. This makes Tariff suitable for import, feed-in, export by either
 * changing the energy data source or calculation function based on tariff type.
 *
 * TBD: block tariffs - although no evidence these will be implemented in the UK
 * TBD: make tariff truely a timeseries for flat rate, tou
 * TBD: make tou vary on a given day / day of the week
 * TBD: look at implications of daylight savings on the data structure definition
 */
export default class Tariff extends mTimeseries.default {
    id: string;
    name: string;
    supplier: string;
    description: string;
    product: string;
    productId: string;
    type: string;
    gsp: string;
    _flat: {
        rate: number;
    };
    _tou: Float64Array;
    exitFees: number;
    term: number;
    green: boolean;
    fuel: string;
    standingcharge: number;
    /**
     * Uses a tariff name and gsp code to load a timeseries representing a dynamic tariff
     * standing charge must be set with a seperate call
     * TDB: add a standing charge component in data structure?
     * TDB: precision on prices is 1/10,000 - is this sufficient?
     * TBD: move this._intervalDecode to Timeseries and make common across all formats
     * @param {*} gsp - grid service point as a letter
     * @param {*} name - tariff name, must be unique
     */
    loadDynamicPrice(params: any): Promise<void>;
    fkid: any;
    /**
     * Sets the flat rate price structure plus standing charge
     * TBD: support start / end date to implement changes in price?
     */
    set flatPrice(arg: {
        rate: number;
    });
    /**
     * Gets the flat rate tariff price structure
     * TBD: support start / end date to implement changes in price?
     */
    get flatPrice(): {
        rate: number;
    };
    /**
     * Sets the time of use prices with standing charge and an array of times with price bands
     * TBD: support start / end date to implement changes in price?
     * TBD: support semantics to set the slots by number rather than date
     */
    set touPrice(arg: any[]);
    /**
     * Returns the time of use tariff structure
     */
    get touPrice(): any[];
    /**
     * Returns the dynamic price structure
     */
    get dynamicPrice(): Float64Array;
    /**
     * Sets the tariff rates based on the structure
     * there is a subtle use of the setter properties
     * above when it looks like a simple assignment
     * in these functions
     *
     * This is async, because the dynamic tariff is loaded
     * from the server
     *
     * TBD: how to adapt to handle different time ranges, maybe this is merge
     * of two instances of Tariff rather than loading up a single structure
     * TBD: start time of tariff? end time?
     * TBD: should this create a 30 minute timeseries in all cases? or too much memory use?
     * TBD: what about other attributes like product, supplier?
     *
     */
    setTariff(tariff: any): Promise<boolean>;
    brand_score: any;
    brand_image: any;
    is_variable: boolean | undefined;
    is_prepay: boolean | undefined;
    is_smart: boolean | undefined;
    is_ev: boolean | undefined;
    is_solar: boolean | undefined;
    is_heatpump: boolean | undefined;
    /**
     * Move to timeseries class to be shared
     * @param {*} intervalCode
     */
    _intervalDecode(intervalCode: any): number | undefined;
    /**
     * DEPRECATE - doesn't allow for tariffs crossing midnight boundary
     * Uses an array of time ranges and unit rates to set the half hourly
     * unit rates. This is fixed and needs to be considered when mixing with
     * consumption for cost calculations.
     * @param {*} touPrices
     */
    _touSetSlotPriceWithDate(touPrices: any): void;
    /**
     * Returns an array of time ranges and rates for TOU data structure
     */
    _touEncode(): any[];
    /**
     * REPLACES _touSetSlotPriceWithDate
     * It extends the date time understanding to previous days
     *
     * Uses an array of time ranges and unit rates to set the half hourly
     * unit rates. This is fixed and needs to be considered when mixing with
     * consumption for cost calculations.
     * @param {*} touPrice
     */
    _touSetSlotPriceWithFullDate(touPrice: any): void;
    /**
     * Experimental - not used
     * For simple setting without a date, using only time
     * Doesn't give good expression of midnight crossing tariff blocks
     * @param {*} data
     */
    _touSetSlotPriceWithHour(data: any): void;
    /**
     * Directly setting the slot price for TOU
     * @param {*} data
     */
    _touSetSlotPrice(data: any): void;
}
import * as mTimeseries from "./timeseries.js";
