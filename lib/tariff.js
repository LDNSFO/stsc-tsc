'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Tariff
 * Class that holds the rates for a tariff
 * Loader brings in tariffs from server or
 * the local methods can be used to load your own single tariff
 * works in conjunction with Products as Tariffs are specific
 * to GSP region
 *
 * Copyright 2020 - 2021 Hildebrand Technology Limit
 * Author: Joshua Cooper - jcooper@hildebrand.co.uk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy o
 * this software and associated documentation files (the "Software"), to deal in th
 * Software without restriction, including without limitation the rights to use
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copie
 * of the Software, and to permit persons to whom the Software is furnishe
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
const axios_1 = require("axios");
const luxon_1 = require("luxon");
const mTimeseries = require("./timeseries.js");
const Timeseries = mTimeseries.default || mTimeseries;
/**
 * Tariff class represents the type, unit rates and some characteristics that are specific
 * to region (GSP code). Contract length, exit fees are captured at this level as well, although
 * some suppliers may model these as Product attributes. It is handy to maintain them here for
 * calculations beyond simple filtering.
 *
 * The point of this class is to store and retrieve data structures so that higher level classes
 * can perform calculations. This makes Tariff suitable for import, feed-in, export by either
 * changing the energy data source or calculation function based on tariff type.
 *
 * TBD: block tariffs - although no evidence these will be implemented in the UK
 * TBD: make tariff truely a timeseries for flat rate, tou
 * TBD: make tou vary on a given day / day of the week
 * TBD: look at implications of daylight savings on the data structure definition
 */
class Tariff extends Timeseries {
    constructor() {
        super();
        // id concept not well used or established, set to fkid at the moment
        // should be a mix of supplier and fkid
        this.id = '';
        this.name = '';
        this.supplier = '';
        this.description = '';
        this.product = '';
        this.productId = '';
        this.type = '';
        this.gsp = '';
        this._flat = {
            rate: 0
        };
        this._tou = new Float64Array(48);
        this.exitFees = 0;
        this.term = 0;
        // TBD - include the carbon mix
        this.green = false;
        // TDB - should label the fuel (gas, electric, etc)
        this.fuel = 'electric';
        // TBD - should label the direction (import, export)
        //       can imagine a mixed product with an import
        //       and export tariff attached
        this.standingcharge = 0;
        this._type = 0x01;
        this._modified = luxon_1.DateTime.utc().ts;
    }
    /**
     * Uses a tariff name and gsp code to load a timeseries representing a dynamic tariff
     * standing charge must be set with a seperate call
     * TDB: add a standing charge component in data structure?
     * TDB: precision on prices is 1/10,000 - is this sufficient?
     * TBD: move this._intervalDecode to Timeseries and make common across all formats
     * @param {*} gsp - grid service point as a letter
     * @param {*} name - tariff name, must be unique
     */
    loadDynamicPrice(params) {
        return __awaiter(this, void 0, void 0, function* () {
            // web call to id
            // then load into structure
            let stscServiceURL = "https://beta.smarttariffsmartcomparison.org/dynamictariff.php";
            this.type = 'dynamic';
            let queryString = '?gsp=' + params.gsp + '&name=' + params.name;
            yield axios_1.default(stscServiceURL + queryString, {
                responseType: 'arraybuffer',
                headers: {
                    'Cache-Control': 'no-store'
                }
            }).then((response) => {
                if (response.status === 200) {
                    this._fetchtime = response.headers.date;
                    this._modified = response.headers['last-modified'];
                    const view = new DataView(response.data);
                    this._type = view.getUint8(0);
                    this._rawinterval = view.getUint8(1);
                    this.start = view.getUint32(2) * 1000;
                    // get the size
                    const numOfElements = parseInt(view.byteLength, 10) - 6;
                    this._rawdata = new Float64Array(parseInt(numOfElements / 4, 10));
                    let j = 0;
                    // precision is 1/10,000 so convert to floating point decimal
                    for (let i = 0; i < numOfElements; i += 4) {
                        this._rawdata[j] = view.getInt32(i + 6) / 10000;
                        j++;
                    }
                    this.interval = this._intervalDecode(this._rawinterval); // seconds
                    this.end = this.start + (this.interval * (j - 1));
                    this.type = 'dynamic';
                    this.fkid = params.name;
                    this.gsp = params.gsp;
                }
            })
                .catch((error) => {
                // console.log(error);
                throw error;
            });
        });
    }
    /**
     * Gets the flat rate tariff price structure
     * TBD: support start / end date to implement changes in price?
     */
    get flatPrice() {
        return { rate: this._flat.rate };
    }
    /**
     * Sets the flat rate price structure plus standing charge
     * TBD: support start / end date to implement changes in price?
     */
    set flatPrice(data) {
        this.type = 'flat';
        this._flat.rate = data.rate;
        this._modified = luxon_1.DateTime.utc().ts;
    }
    /**
     * Returns the time of use tariff structure
     */
    get touPrice() {
        return this._touEncode();
    }
    /**
     * Sets the time of use prices with standing charge and an array of times with price bands
     * TBD: support start / end date to implement changes in price?
     * TBD: support semantics to set the slots by number rather than date
     */
    set touPrice(data) {
        this.type = 'tou';
        //this._touSetSlotPrice(data);
        this._touSetSlotPriceWithFullDate(data);
        this._modified = luxon_1.DateTime.utc().ts;
    }
    /**
     * Returns the dynamic price structure
     */
    get dynamicPrice() {
        return this._tou;
    }
    /**
     * Sets the tariff rates based on the structure
     * there is a subtle use of the setter properties
     * above when it looks like a simple assignment
     * in these functions
     *
     * This is async, because the dynamic tariff is loaded
     * from the server
     *
     * TBD: how to adapt to handle different time ranges, maybe this is merge
     * of two instances of Tariff rather than loading up a single structure
     * TBD: start time of tariff? end time?
     * TBD: should this create a 30 minute timeseries in all cases? or too much memory use?
     * TBD: what about other attributes like product, supplier?
     *
     */
    setTariff(tariff) {
        return __awaiter(this, void 0, void 0, function* () {
            this.id = tariff.fkid;
            this.name = tariff.name;
            this.standingcharge = tariff.standingcharge;
            this.start = luxon_1.DateTime.fromISO(tariff.start).ts;
            this.gsp = tariff.gsp;
            this.brand_score = tariff.brand_score;
            this.brand_image = tariff.brand_image;
            if (tariff.is_green)
                this.green = true;
            this.is_variable = false;
            if (tariff.is_variable)
                this.is_variable = true;
            if (tariff.is_prepay)
                this.is_prepay = true;
            if (tariff.is_smart)
                this.is_smart = true;
            this.term = tariff.term;
            if (tariff.type === 'tou')
                this.is_ev = true;
            if (tariff.type === 'dynamic')
                this.is_ev = true;
            if (tariff.type === 'tou')
                this.is_solar = true;
            if (tariff.type === 'dynamic')
                this.is_solar = true;
            if (tariff.type === 'tou')
                this.is_heatpump = true;
            if (tariff.type === 'dynamic')
                this.is_heatpump = true;
            if (tariff.type === 'flat') {
                this.flatPrice = {
                    start: tariff.available_from,
                    rate: tariff.rate.unitrate
                };
            }
            else if (tariff.type === 'tou') {
                this.touPrice = tariff.rate;
            }
            else if (tariff.type === 'dynamic') {
                yield this.loadDynamicPrice({ name: tariff.fkid, gsp: tariff.gsp });
                let end = new Date().setUTCHours(0, 0, 0, 0);
                let start = end - (100 * 24 * 60 * 60 * 1000);
                let lasthundreddaysDynamicPrice = this.range({
                    start: start,
                    end: end,
                    interval: 1800000
                });
                this._tou = lasthundreddaysDynamicPrice.byHHSlot({ aggregateFunction: Timeseries.AGG_MEAN });
            }
            return true;
        });
    }
    /**
     * Move to timeseries class to be shared
     * @param {*} intervalCode
     */
    _intervalDecode(intervalCode) {
        if (intervalCode === 0x0F)
            return (30 * 60 * 1000);
    }
    /**
     * DEPRECATE - doesn't allow for tariffs crossing midnight boundary
     * Uses an array of time ranges and unit rates to set the half hourly
     * unit rates. This is fixed and needs to be considered when mixing with
     * consumption for cost calculations.
     * @param {*} touPrices
     */
    _touSetSlotPriceWithDate(touPrices) {
        // touPrices that is passed is a start datetime, end datetime and price 
        // slots index (0-47)
        touPrices.forEach((price) => {
            let startSlot = luxon_1.DateTime.fromISO(price.valid_from).hour * 2;
            if (luxon_1.DateTime.fromISO(price.valid_from).minute > 29) {
                startSlot++;
            }
            ;
            let endSlot = luxon_1.DateTime.fromISO(price.valid_to).hour * 2;
            if (luxon_1.DateTime.fromISO(price.valid_to).minute > 29) {
                endSlot++;
            }
            ;
            for (let slot = startSlot; slot < endSlot; slot++) {
                this._tou[slot] = price.value_inc_vat;
            }
        });
    }
    /**
     * Returns an array of time ranges and rates for TOU data structure
     */
    _touEncode() {
        let today = luxon_1.DateTime.now().startOf('day');
        let output = [];
        this._tou.forEach((hh) => {
            output.push({ valid_from: today.toISO(), valid_to: today.plus({ hours: 0.5 }).toISO(), value_inc_vat: hh });
            today = today.plus({ hours: 0.5 });
        });
        return output;
    }
    /**
     * REPLACES _touSetSlotPriceWithDate
     * It extends the date time understanding to previous days
     *
     * Uses an array of time ranges and unit rates to set the half hourly
     * unit rates. This is fixed and needs to be considered when mixing with
     * consumption for cost calculations.
     * @param {*} touPrice
     */
    _touSetSlotPriceWithFullDate(touPrice) {
        // touPrice that is passed is a start datetime, end datetime and price 
        // slots index (0-47)
        // TBD - look at the not contigous regions!
        //console.log(touPrice);
        touPrice.forEach((price) => {
            // look to see if on same day
            let startTime = luxon_1.DateTime.fromISO(price.valid_from);
            let endTime = luxon_1.DateTime.fromISO(price.valid_to);
            if (startTime.day < endTime.day) {
                // take start day from point given up to midnight (slot 47)
                let startSlot = startTime.hour * 2;
                if (luxon_1.DateTime.fromISO(price.valid_from).minute > 29) {
                    startSlot++;
                }
                ;
                let endSlot = 48;
                for (let slot = startSlot; slot < endSlot; slot++) {
                    this._tou[slot] = price.value_inc_vat;
                }
                // then go back in at the start (mod 47)
                startSlot = 0;
                endSlot = endTime.hour * 2;
                if (luxon_1.DateTime.fromISO(price.valid_to).minute > 29) {
                    endSlot++;
                }
                ;
                for (let slot = startSlot; slot < endSlot; slot++) {
                    this._tou[slot] = price.value_inc_vat;
                }
            }
            else {
                let startSlot = startTime.hour * 2;
                if (luxon_1.DateTime.fromISO(price.valid_from).minute > 29) {
                    startSlot++;
                }
                ;
                let endSlot = endTime.hour * 2;
                if (luxon_1.DateTime.fromISO(price.valid_to).minute > 29) {
                    endSlot++;
                }
                ;
                for (let slot = startSlot; slot < endSlot; slot++) {
                    this._tou[slot] = price.value_inc_vat;
                }
            }
        });
    }
    /**
     * Experimental - not used
     * For simple setting without a date, using only time
     * Doesn't give good expression of midnight crossing tariff blocks
     * @param {*} data
     */
    _touSetSlotPriceWithHour(data) {
        // data that is passed is a start hour:min, end hour:min and price 
        // slots index (0-47)
        let startSlot = luxon_1.DateTime.fromISO(data.start).hour * 2;
        if (luxon_1.DateTime.fromISO(data.start).minute > 29) {
            startSlot++;
        }
        ;
        let endSlot = luxon_1.DateTime.fromISO(data.end).hour * 2;
        if (luxon_1.DateTime.fromISO(data.end).minute > 29) {
            endSlot++;
        }
        ;
        for (let slot = startSlot; slot < endSlot; slot++) {
            this._tou[slot] = data.rate;
        }
    }
    /**
     * Directly setting the slot price for TOU
     * @param {*} data
     */
    _touSetSlotPrice(data) {
        // data that is passed is a start, end and price 
        // slots index (0-47)
        if (data.start < 0 || data.start > 47) {
            throw new Error("Start slot out of range (0-47)");
        }
        if (data.end > 48 || data.end < 1) {
            throw new Error("End slot out of range (1-48)");
        }
        for (let slot = data.start; slot < data.end; slot++) {
            this._tou[slot] = data.rate;
        }
    }
}
exports.default = Tariff;
