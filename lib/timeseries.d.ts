/**
 *   Timeseries - An abstract class that will get it semantics from child classes.
 *   Loading data is taken care of by the instance class as each type of data has different
 *   sources and structures.
 *
 *   Visualisation libraries can use the data source - Highcharts is assumed to be the library
 */
export default class Timeseries {
    /**
     * Used to signify the aggregate sum function as applied to a timeseries
     * it is the function called when reducing a time series into
     * intervals
     *
     * The other aggreate functions work the same way and are called
     * as Timeseries.AGG_SUM within a parameter list
     */
    static get AGG_SUM(): number;
    /**
     * used to signify the aggregate mean or average function
     */
    static get AGG_MEAN(): number;
    /**
     * used to signify the aggregate variance function
     * this is variance within the set of data being reduced
     */
    static get AGG_VAR(): number;
    /**
     * used to signify the aggregate maximum function
     */
    static get AGG_MAX(): number;
    /**
     * used to signify the aggregate minimum function
     */
    static get AGG_MIN(): number;
    /**
     * used to signify the aggregate median function
     */
    static get AGG_MEDIAN(): number;
    /**
     * used to signify the aggregate count function
     */
    static get AGG_COUNT(): number;
    /**
     * used to signify the aggregate standard deviaton function
     */
    static get AGG_STDDEV(): number;
    /**
     * NOT implemented - reserved word for future
     */
    static get AGG_VELOCITY(): number;
    /**
     * NOT implemented - reserved word for future
     */
    static get AGG_ACCELERATION(): number;
    start: number;
    end: number;
    interval: number;
    isTimeseries: boolean;
    _modified: number;
    _rawdata: any[];
    _rawinterval: number;
    _fetchtime: any;
    _type: number;
    _cacheModified: number;
    _cachedCount: number;
    _cachedSum: number;
    _cachedMax: number;
    _cachedMin: number;
    _cachedNzmin: number;
    _cachedMean: number;
    _cachedMedian: number;
    _cachedStddev: number;
    _cachedVariance: number;
    _cachedHistogram: any[];
    _cachedByDOW: any[];
    _cachedByHHSlot: any[];
    _cachedByHHSlotLocaltime: any[];
    _cachedByHHSlotDOW: any[];
    _cachedByHHSlotDOWLocaltime: any[];
    _cachedByMonth: any[];
    /**
     * Take an ISO time to set the start epoch milliseconds
     */
    set from(arg: string);
    /**
     * Reformat start (epoch milliseconds) into an ISO time
     */
    get from(): string;
    /**
     * Take an ISO time to set the end epoch milliseconds
     */
    set to(arg: string);
    /**
     * Reformat end (epoch milliseconds) into an ISO time
     */
    get to(): string;
    /**
     * .data as a property is set with an array of coordinates
     * [timestamp, value]
     * TBD: look at how the interval is calculated, it may need validation
     *
     */
    set data(arg: any[]);
    /**
     * .data as a property returns an array rather than a new timeseries
     * object. The array is of coordinates [timestamp, value]
     */
    get data(): any[];
    set slotdata(arg: any[]);
    /**
     * Slot data is direct access to the data within the storage slots
     * so there is no context as to start or end timestamp
     */
    get slotdata(): any[];
    range(param: any): any;
    count(): number;
    nzmin(): number;
    max(): number;
    mean(): number;
    median(): number;
    min(): number;
    stddev(): number;
    sum(): number;
    variance(): number;
    /**
     * Returns an array with the index as the value - therefore only integer values
     * and the count of occurence as a value
     * TBD: binning
     * TBD: more flexible data structure
     * TBD: floating point
     */
    histogram(): any[];
    /**
     * Multiply a scaler times each value in the timeseries (amplify or dampen)
     * @param {*} a
     */
    scale(a: any): void;
    /**
     * Add a scaler number to each value in the timeseries (translate up or down)
     * @param {*} b
     */
    translate(b: any): void;
    /**
     * This may be redundant as .data will do the same thing
     * originally wanted a way to format to CSV, but that is probably
     * not possible via a JS library
     * May want to have formatter options - ISO, epoch, seconds, milliseconds, localtime, etc
     */
    export(): any[];
    /**
     * Add more data points on to the end of the timeseries
     *
     * @param {*} data coordinate of timepoint and
     */
    append(data: any): void;
    /**
     * Summarise the data month by month using an aggregate function
     * This is used instead of P1M summary as Luxon seems to not recognise
     * monthly boundaries with the built in ISO duration
     * This ensures start and end control for each month
     */
    byMonth(param: any): any[];
    /**
     * Uses localtime as half hourly indicator rather than the UTC
     * or epoch time
     * This means that there will not be an equal number of values in
     * the half hour, but if a mean or other aggregation function is
     * being used, this is not an issue
     * TBD: make resilient for zero values in data source - i.e. no data for a period
     * @param {*} aggregateFunction
     */
    byHHSlotLocaltime(param: any): any[];
    /**
     * Using epoch, UTC or continous real time to place a reading into a half hourly
     * slot. The byHHSlotLocaltime is preferred if analysing or visualising behaviour
     * @param {*} aggregateFunction
     */
    byHHSlot(param: any): any[];
    /**
     * Aggregate by day of the week
     * the aggregate function can be passed in, but warning, should probably
     * only use mean, std or variance as they normalise if there are days
     * of the week missing in the time range captured by the function
     * This function can be used when looking at several months of data
     * to see what the highest day of the week usage is
     * returns an array with Monday indexed to zero, Sunday is index 6
     *
     * TBD: allow for start and end dates to be applied (or call range function first)
     *
     * @param {*} aggregateFunction
     */
    byDOW(param: any): any[];
    /**
     * Half hourly by day of the week
     * @param {*} aggregateFunction
     */
    byHHSlotDOW(param: any): any[];
    /**
     * Half hourly by day of the week in localtime
     * @param {*} aggregateFunction
     */
    byHHSlotDOWLocaltime(param: any): any[];
    /**
     *
     * @param {*} start in epoch milliseconds
     * @param {*} end in epoch milliseconds
     * @param {*} interval in milliseconds
     * @param {*} aggregateFunction
     * @param {*} validate boolean
     */
    _range(param: any): any[];
    /**
     * Decodes the function macro into a function
     * @param {*} code
     */
    _decodeAggregate(code: any): ((list: any) => any) | undefined;
    /**
     * Helper function for testing
     */
    _twoDData(): any[];
}
