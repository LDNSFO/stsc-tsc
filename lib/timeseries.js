/**
 * Timeseries
 * Base class that makes working with timeseries common across objects
 *
 * Copyright 2020 - 2021 Hildebrand Technology Limit
 * Author: Joshua Cooper - jcooper@hildebrand.co.uk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy o
 * this software and associated documentation files (the "Software"), to deal in th
 * Software without restriction, including without limitation the rights to use
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copie
 * of the Software, and to permit persons to whom the Software is furnishe
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const utils_js_1 = require("./utils.js");
const luxon_1 = require("luxon");
/**
 *   Timeseries - An abstract class that will get it semantics from child classes.
 *   Loading data is taken care of by the instance class as each type of data has different
 *   sources and structures.
 *
 *   Visualisation libraries can use the data source - Highcharts is assumed to be the library
 */
class Timeseries {
    /**
     * Used to signify the aggregate sum function as applied to a timeseries
     * it is the function called when reducing a time series into
     * intervals
     *
     * The other aggreate functions work the same way and are called
     * as Timeseries.AGG_SUM within a parameter list
     */
    static get AGG_SUM() { return 0; }
    /**
     * used to signify the aggregate mean or average function
     */
    static get AGG_MEAN() { return 1; }
    /**
     * used to signify the aggregate variance function
     * this is variance within the set of data being reduced
     */
    static get AGG_VAR() { return 2; }
    /**
     * used to signify the aggregate maximum function
     */
    static get AGG_MAX() { return 3; }
    /**
     * used to signify the aggregate minimum function
     */
    static get AGG_MIN() { return 4; }
    /**
     * used to signify the aggregate median function
     */
    static get AGG_MEDIAN() { return 5; }
    /**
     * used to signify the aggregate count function
     */
    static get AGG_COUNT() { return 6; }
    /**
     * used to signify the aggregate standard deviaton function
     */
    static get AGG_STDDEV() { return 7; }
    /**
     * NOT implemented - reserved word for future
     */
    static get AGG_VELOCITY() { return 8; }
    /**
     * NOT implemented - reserved word for future
     */
    static get AGG_ACCELERATION() { return 9; }
    constructor() {
        this.start = 0;
        this.end = 0;
        this.interval = 0;
        this.isTimeseries = true;
        this._modified = 0;
        this._rawdata = [];
        this._rawinterval = 0x0F;
        this._fetchtime = luxon_1.DateTime.utc().ts;
        this._type = 0x00;
        this._cacheModified = 0;
        this._cachedCount = 0;
        this._cachedSum = 0;
        this._cachedMax = 0;
        this._cachedMin = 0;
        this._cachedNzmin = 0;
        this._cachedMean = 0;
        this._cachedMedian = 0;
        this._cachedStddev = 0;
        this._cachedVariance = 0;
        this._cachedHistogram = [];
        this._cachedByDOW = [];
        this._cachedByHHSlot = [];
        this._cachedByHHSlotLocaltime = [];
        this._cachedByHHSlotDOW = [];
        this._cachedByHHSlotDOWLocaltime = [];
        this._cachedByMonth = [];
    }
    /**
     * Reformat start (epoch milliseconds) into an ISO time
     */
    get from() {
        return luxon_1.DateTime.fromMillis(this.start).toISO();
    }
    /**
     * Take an ISO time to set the start epoch milliseconds
     */
    set from(datetime) {
        this.start = luxon_1.DateTime.fromISO(datetime);
        this._modified = luxon_1.DateTime.utc().ts;
    }
    /**
     * Reformat end (epoch milliseconds) into an ISO time
     */
    get to() {
        return luxon_1.DateTime.fromMillis(this.end).toISO();
    }
    /**
     * Take an ISO time to set the end epoch milliseconds
     */
    set to(datetime) {
        this.end = luxon_1.DateTime.fromISO(datetime);
        this._modified = luxon_1.DateTime.utc().ts;
    }
    /**
     * .data as a property returns an array rather than a new timeseries
     * object. The array is of coordinates [timestamp, value]
     */
    get data() {
        return this._range({
            start: this.start,
            end: (this.end + 1),
            interval: this.interval,
        });
    }
    /**
     * .data as a property is set with an array of coordinates
     * [timestamp, value]
     * TBD: look at how the interval is calculated, it may need validation
     *
     */
    set data(data) {
        // times should be in milliseconds, including interval
        this.start = data[0][0];
        this.end = data[data.length - 1][0];
        this.interval = data[1][0] - data[0][0];
        this._rawdata = new Float64Array(data.length);
        // TBD: order by time to make sure last value is last time
        // TBD: check the interval on the time stamps
        let i = 0;
        data.forEach((coordinate) => {
            this._rawdata[i] = coordinate[1];
            i++;
        });
        this._modified = luxon_1.DateTime.utc().ts;
    }
    /**
     * Slot data is direct access to the data within the storage slots
     * so there is no context as to start or end timestamp
     */
    get slotdata() {
        return this._rawdata;
    }
    // TBD - change to only take the values in an array, not coordinates
    set slotdata(data) {
        // write consecutive values and let the higher level object set time parameters
        this._rawdata = new Float64Array(data.length);
        let i = 0;
        data.forEach((coordinate) => {
            this._rawdata[i] = parseFloat(coordinate[1]);
            i++;
        });
        this._modified = luxon_1.DateTime.utc().ts;
    }
    range(param) {
        let newRange;
        // let tsRange = new Timeseries();
        let tsRange = new this.constructor();
        // TBD: check all of the parameters have been sent
        // decode the operation from the aggregate
        let options = {
            start: 0,
            end: 0,
            from: '',
            to: '',
            interval: 1800000,
            intervalISO: '',
            aggregateFunction: 0
        };
        options = Object.assign(Object.assign({}, options), param);
        if (options.from.length > 9) {
            options.start = luxon_1.DateTime.fromISO(options.from).ts;
        }
        if (options.to.length > 9) {
            options.end = luxon_1.DateTime.fromISO(options.to).ts;
        }
        if (options.intervalISO.length > 0) {
            options.interval = luxon_1.Duration.fromISO(options.intervalISO).toMillis();
            if (!(options.interval)) {
                throw new Error('ISO Interval string is invalid');
            }
        }
        newRange = this._range(options);
        if (newRange.length > 0) {
            tsRange.data = newRange;
        }
        return tsRange;
    }
    // summary operators on whole of Timeseries
    count() {
        if (this._modified >= this._cacheModified || this._cachedCount === 0) {
            this._cachedCount = this._rawdata.length;
            this._cacheModified = luxon_1.DateTime.utc().ts;
        }
        return this._cachedCount;
    }
    nzmin() {
        if (this._modified >= this._cacheModified || this._cachedNzmin === 0) {
            this._cachedNzmin = utils_js_1.default._nzmin(this._rawdata);
            this._cacheModified = luxon_1.DateTime.utc().ts;
        }
        return this._cachedNzmin;
    }
    max() {
        if (this._modified >= this._cacheModified || this._cachedMax === 0) {
            this._cachedMax = utils_js_1.default._max(this._rawdata);
            this._cacheModified = luxon_1.DateTime.utc().ts;
        }
        return this._cachedMax;
    }
    mean() {
        if (this._modified >= this._cacheModified || this._cachedMean === 0) {
            this._cachedMean = utils_js_1.default._mean(this._rawdata);
            this._cacheModified = luxon_1.DateTime.utc().ts;
        }
        return this._cachedMean;
    }
    median() {
        if (this._modified >= this._cacheModified || this._cachedMedian === 0) {
            this._cachedMedian = utils_js_1.default._median(this._rawdata);
            this._cacheModified = luxon_1.DateTime.utc().ts;
        }
        return this._cachedMedian;
    }
    min() {
        if (this._modified >= this._cacheModified || this._cachedMin === 0) {
            this._cachedMin = utils_js_1.default._min(this._rawdata);
            this._cacheModified = luxon_1.DateTime.utc().ts;
        }
        return this._cachedMin;
    }
    stddev() {
        if (this._modified >= this._cacheModified || this._cachedStddev === 0) {
            this._cachedStddev = utils_js_1.default._standardDeviation(this._rawdata);
            this._cacheModified = luxon_1.DateTime.utc().ts;
        }
        return this._cachedStddev;
    }
    sum() {
        if (this._modified >= this._cacheModified || this._cachedSum === 0) {
            this._cachedSum = utils_js_1.default._sum(this._rawdata);
            this._cacheModified = luxon_1.DateTime.utc().ts;
        }
        return this._cachedSum;
    }
    variance() {
        if (this._modified >= this._cacheModified || this._cachedVariance === 0) {
            this._cachedVariance = utils_js_1.default._variance(this._rawdata);
            this._cacheModified = luxon_1.DateTime.utc().ts;
        }
        return this._cachedVariance;
    }
    /**
     * Returns an array with the index as the value - therefore only integer values
     * and the count of occurence as a value
     * TBD: binning
     * TBD: more flexible data structure
     * TBD: floating point
     */
    histogram() {
        if (this._modified >= this._cacheModified || this._cachedHistogram.length === 0) {
            this._cachedHistogram = utils_js_1.default._histogram(this._rawdata);
            this._cacheModified = luxon_1.DateTime.utc().ts;
        }
        return this._cachedHistogram;
    }
    /**
     * Multiply a scaler times each value in the timeseries (amplify or dampen)
     * @param {*} a
     */
    scale(a) {
        this._rawdata = this._rawdata.map((item) => { return item * a; });
        this._modified = luxon_1.DateTime.utc().ts;
    }
    /**
     * Add a scaler number to each value in the timeseries (translate up or down)
     * @param {*} b
     */
    translate(b) {
        this._rawdata = this._rawdata.map((item) => { return item + b; });
        this._modified = luxon_1.DateTime.utc().ts;
    }
    /**
     * This may be redundant as .data will do the same thing
     * originally wanted a way to format to CSV, but that is probably
     * not possible via a JS library
     * May want to have formatter options - ISO, epoch, seconds, milliseconds, localtime, etc
     */
    export() {
        let i = this.start;
        let output = [];
        this._rawdata.forEach((element) => {
            //console.log(i + "," + element);
            output.push({ ts: i, value: element });
            i += this.interval;
        });
        return output;
    }
    /**
     * Add more data points on to the end of the timeseries
     *
     * @param {*} data coordinate of timepoint and
     */
    append(data) {
        let indexTS = this.end + this.interval;
        this._modified = luxon_1.DateTime.utc().ts;
        // test to see if timestamp is correct
        data.forEach((coordinate) => {
            if (coordinate[0] === indexTS) {
                this._rawdata.push(coordinate[1]);
                indexTS += this.interval;
            }
            else {
                throw new Error("Can not append before end of timeseries");
            }
        });
    }
    /**
     * Summarise the data month by month using an aggregate function
     * This is used instead of P1M summary as Luxon seems to not recognise
     * monthly boundaries with the built in ISO duration
     * This ensures start and end control for each month
     */
    byMonth(param) {
        let months = [];
        let monthExtract;
        if (this._modified >= this._cacheModified || this._cachedByMonth.length === 0) {
            // TBD: flag for watt hours or kWh, Wh by default
            // TBD: start time and end time needs to be looked at to make sure that ragne doesn't fail asking
            // for data beyond the bounds
            // TBD: line 403 don't sum
            // if we want to have an arbitrary from / to date then this is a way to do it
            // let startThisMonth = DateTime.utc().startOf('month');
            // let params = { from: '2020-01-01T00:00:00Z', to: '2021-02-01T00:00:00Z' };
            // let howManyMonths = Interval.fromDateTimes(DateTime.fromISO(params.from), DateTime.fromISO(params.to)).count('months');
            // startThisMonth = DateTime.fromISO(params.to).startOf('month');
            let durationMonth = luxon_1.Duration.fromISO('P1M');
            // let startOfMonth = DateTime.utc().startOf('month');
            let startOfMonth = luxon_1.DateTime.fromMillis(this.end + this.interval).toUTC().startOf('month');
            let aggregateFunction = this._decodeAggregate(Timeseries.AGG_SUM);
            if (param) {
                if (param.aggregateFunction) {
                    aggregateFunction = this._decodeAggregate(param.aggregateFunction);
                }
            }
            // fix to most recent 12 months 
            let howManyMonths = 12;
            for (let j = 0; j < (howManyMonths); j++) {
                // always takes the last whole month
                // use AGG_SUM to return all of the values as this function is to clip at a monthly level
                monthExtract = this.range({ start: luxon_1.Interval.before(startOfMonth, durationMonth).s.ts, end: luxon_1.Interval.before(startOfMonth, durationMonth).e.ts, aggregateFunction: Timeseries.AGG_SUM, interval: luxon_1.Duration.fromISO('PT30M') });
                // TBD: sum???
                months.push([luxon_1.Interval.before(startOfMonth, durationMonth).s.ts, monthExtract.sum()]);
                startOfMonth = startOfMonth.minus(durationMonth);
            }
            this._cachedByMonth = months.reverse();
            this._cacheModified = luxon_1.DateTime.utc().ts;
        }
        return this._cachedByMonth;
    }
    /**
     * Uses localtime as half hourly indicator rather than the UTC
     * or epoch time
     * This means that there will not be an equal number of values in
     * the half hour, but if a mean or other aggregation function is
     * being used, this is not an issue
     * TBD: make resilient for zero values in data source - i.e. no data for a period
     * @param {*} aggregateFunction
     */
    byHHSlotLocaltime(param) {
        if (this._modified >= this._cacheModified || this._cachedByHHSlotLocaltime.length === 0) {
            let reducedData = [];
            // this will be a 2D array of 48 x number of days in the data set
            let workingData = [];
            for (let i = 0; i < 48; i++) {
                workingData[i] = [];
            }
            let aggregateFunction = this._decodeAggregate(Timeseries.AGG_SUM);
            if (param) {
                if (param.aggregateFunction) {
                    aggregateFunction = this._decodeAggregate(param.aggregateFunction);
                }
            }
            let indexTS = this.start;
            let slot = luxon_1.DateTime.fromMillis(indexTS).hour * 2;
            if (luxon_1.DateTime.fromMillis(indexTS).minute > 29) {
                slot++;
            }
            ;
            this._rawdata.forEach((value) => {
                workingData[parseInt(slot, 10)].push(value);
                // 30 minutes fixed as it is half hour
                indexTS += 1800000;
                slot = luxon_1.DateTime.fromMillis(indexTS).hour * 2;
                if (luxon_1.DateTime.fromMillis(indexTS).minute > 29) {
                    slot++;
                }
                ;
            });
            for (let i = 0; i < 48; i++) {
                reducedData[i] = aggregateFunction(workingData[i]);
            }
            this._cachedByHHSlotLocaltime = reducedData;
            this._cacheModified = luxon_1.DateTime.utc().ts;
        }
        return this._cachedByHHSlotLocaltime;
    }
    /**
     * Using epoch, UTC or continous real time to place a reading into a half hourly
     * slot. The byHHSlotLocaltime is preferred if analysing or visualising behaviour
     * @param {*} aggregateFunction
     */
    byHHSlot(param) {
        if (this._modified >= this._cacheModified || this._cachedByHHSlot.length === 0) {
            let reducedData = [];
            // this will be a 2D array of 48 x number of days in the data set
            let workingData = [];
            for (let i = 0; i < 48; i++) {
                workingData[i] = [];
            }
            let aggregateFunction = this._decodeAggregate(Timeseries.AGG_SUM);
            if (param) {
                if (param.aggregateFunction) {
                    aggregateFunction = this._decodeAggregate(param.aggregateFunction);
                }
            }
            let startSlot = luxon_1.DateTime.fromMillis(this.start).hour * 2;
            if (luxon_1.DateTime.fromMillis(this.start).minute > 29) {
                startSlot++;
            }
            ;
            let slot = startSlot;
            this._rawdata.forEach((value) => {
                workingData[parseInt(slot, 10)].push(value);
                if (slot < 47) {
                    slot++;
                }
                else {
                    slot = 0;
                }
            });
            for (let i = 0; i < 48; i++) {
                reducedData[i] = aggregateFunction(workingData[i]);
            }
            this._cachedByHHSlot = reducedData;
            this._cacheModified = luxon_1.DateTime.utc().ts;
        }
        return this._cachedByHHSlot;
    }
    /**
     * Aggregate by day of the week
     * the aggregate function can be passed in, but warning, should probably
     * only use mean, std or variance as they normalise if there are days
     * of the week missing in the time range captured by the function
     * This function can be used when looking at several months of data
     * to see what the highest day of the week usage is
     * returns an array with Monday indexed to zero, Sunday is index 6
     *
     * TBD: allow for start and end dates to be applied (or call range function first)
     *
     * @param {*} aggregateFunction
     */
    byDOW(param) {
        if (this._modified >= this._cacheModified || this._cachedByDOW.length === 0) {
            let reducedData = [];
            // this will be a 2D array of 7 x number of days in the data set
            let workingData = [];
            for (let i = 0; i < 7; i++) {
                workingData[i] = [];
            }
            let aggregateFunction = this._decodeAggregate(Timeseries.AGG_SUM);
            if (param) {
                if (param.aggregateFunction) {
                    aggregateFunction = this._decodeAggregate(param.aggregateFunction);
                }
            }
            let slotTs = this.start;
            let slot = luxon_1.DateTime.fromMillis(slotTs).weekday - 1;
            this._rawdata.forEach((value) => {
                workingData[parseInt(slot, 10)].push(value);
                slotTs += this.interval;
                slot = luxon_1.DateTime.fromMillis(slotTs).weekday - 1;
            });
            for (let i = 0; i < 7; i++) {
                reducedData[i] = aggregateFunction(workingData[i]);
            }
            this._cachedByDOW = reducedData;
            this._cacheModified = luxon_1.DateTime.utc().ts;
        }
        return this._cachedByDOW;
    }
    /**
     * Half hourly by day of the week
     * @param {*} aggregateFunction
     */
    byHHSlotDOW(param) {
        if (this._modified >= this._cacheModified || this._cachedByHHSlotDOW.length === 0) {
            let reducedData = [];
            // this will be a 3D array of 7 x 48 x number of days in the data set
            let workingData = [];
            for (let i = 0; i < 7; i++) {
                workingData[i] = [];
                reducedData[i] = [];
                for (let j = 0; j < 48; j++) {
                    workingData[i][j] = [];
                    reducedData[i][j] = 0;
                }
            }
            let aggregateFunction = this._decodeAggregate(Timeseries.AGG_SUM);
            if (param) {
                if (param.aggregateFunction) {
                    aggregateFunction = this._decodeAggregate(param.aggregateFunction);
                }
            }
            let slotTs = this.start;
            let daySlot = luxon_1.DateTime.fromMillis(slotTs).weekday - 1;
            let hhSlot = luxon_1.DateTime.fromMillis(slotTs).hour * 2;
            if (luxon_1.DateTime.fromMillis(slotTs).minute > 29) {
                hhSlot++;
            }
            ;
            this._rawdata.forEach((value) => {
                workingData[parseInt(daySlot, 10)][parseInt(hhSlot, 10)].push(value);
                slotTs += this.interval;
                daySlot = luxon_1.DateTime.fromMillis(slotTs).weekday - 1;
                if (hhSlot < 47) {
                    hhSlot++;
                }
                else {
                    hhSlot = 0;
                }
            });
            for (let i = 0; i < 7; i++) {
                for (let j = 0; j < 48; j++) {
                    reducedData[i][j] = Math.round(aggregateFunction(workingData[i][j]));
                }
            }
            this._cachedByHHSlotDOW = reducedData;
        }
        return this._cachedByHHSlotDOW;
    }
    /**
     * Half hourly by day of the week in localtime
     * @param {*} aggregateFunction
     */
    byHHSlotDOWLocaltime(param) {
        if (this._modified >= this._cacheModified || this._cachedByHHSlotDOWLocaltime.length === 0) {
            let reducedData = [];
            // this will be a 3D array of 7 x 48 x number of days in the data set
            let workingData = [];
            for (let i = 0; i < 7; i++) {
                workingData[i] = [];
                reducedData[i] = [];
                for (let j = 0; j < 48; j++) {
                    workingData[i][j] = [];
                    reducedData[i][j] = 0;
                }
            }
            let aggregateFunction = this._decodeAggregate(Timeseries.AGG_SUM);
            if (param) {
                if (param.aggregateFunction) {
                    aggregateFunction = this._decodeAggregate(param.aggregateFunction);
                }
            }
            let indexTS = this.start;
            let slot = luxon_1.DateTime.fromMillis(indexTS).hour * 2;
            let daySlot = luxon_1.DateTime.fromMillis(indexTS).weekday - 1;
            if (luxon_1.DateTime.fromMillis(indexTS).minute > 29) {
                slot++;
            }
            ;
            this._rawdata.forEach((value) => {
                workingData[parseInt(daySlot, 10)][parseInt(slot, 10)].push(value);
                // 30 minutes fixed as it is half hour
                indexTS += 1800000;
                daySlot = luxon_1.DateTime.fromMillis(indexTS).weekday - 1;
                slot = luxon_1.DateTime.fromMillis(indexTS).hour * 2;
                if (luxon_1.DateTime.fromMillis(indexTS).minute > 29) {
                    slot++;
                }
                ;
            });
            // this._rawdata.forEach((value) => {
            //     workingData[parseInt(daySlot, 10)][parseInt(slot, 10)].push(value);
            //     indexTS += this.interval;
            //     daySlot = DateTime.fromMillis(indexTS).weekday - 1;
            //     if(slot < 47) {
            //         slot++;
            //     } else {
            //         slot = 0;
            //     }
            // });
            for (let i = 0; i < 7; i++) {
                for (let j = 0; j < 48; j++) {
                    reducedData[i][j] = Math.round(aggregateFunction(workingData[i][j]));
                }
            }
            this._cachedByHHSlotDOWLocaltime = reducedData;
        }
        return this._cachedByHHSlotDOWLocaltime;
    }
    // private 
    /**
     *
     * @param {*} start in epoch milliseconds
     * @param {*} end in epoch milliseconds
     * @param {*} interval in milliseconds
     * @param {*} aggregateFunction
     * @param {*} validate boolean
     */
    _range(param) {
        let start = param.start;
        let end = param.end;
        let interval = param.interval;
        let aggregateFunction = this._decodeAggregate(param.aggregateFunction);
        let validate = false;
        if (param.validate !== undefined) {
            validate = param.validate;
        }
        // DateTime objects representing the start and end
        let datetimeStart;
        let datetimeEnd;
        let outputArray = [];
        if (start > end) {
            throw new Error('Start time is greater than end time');
            return outputArray;
        }
        if (start < this.start) {
            datetimeStart = luxon_1.DateTime.fromMillis(this.start);
            // default is to return something that starts at the start of the data
            // if the range start is less than the data start
            // if strict and want to validate query set validate = true
            if (validate === true) {
                throw new Error('Asking for range before start of data');
            }
        }
        else {
            datetimeStart = luxon_1.DateTime.fromMillis(start);
        }
        if (end > (this.end + this.interval)) {
            datetimeEnd = luxon_1.DateTime.fromMillis(this.end);
            // default is to return something that ends at the end of the data
            // if the range end is greater than the data end
            // if strict and want to validate query set validate = true
            if (validate === true) {
                throw new Error('Asking for range beyond end');
            }
        }
        else {
            datetimeEnd = luxon_1.DateTime.fromMillis(end);
        }
        let startElement = (datetimeStart.ts - this.start) / (this.interval);
        let endElement = ((datetimeEnd.ts - datetimeStart.ts) / (this.interval)) + startElement;
        let skipElement = interval / (this.interval);
        // console.log(startElement);
        // console.log(endElement);
        // console.log(skipElement);
        let list = [];
        for (let i = startElement; i < endElement; i++) {
            // just return don't apply an aggregate function
            if (skipElement === 1) {
                outputArray.push([((i * this.interval) + this.start), this._rawdata[i]]);
            }
            else {
                // aggregate with the values that are in the interval
                // Error - there is an interval missing at the end; premature stop of iteration?
                list.push(this._rawdata[i]);
                if (((i - startElement + 1) % skipElement) === 0) {
                    outputArray.push([(((i - list.length + 1) * this.interval) + this.start), aggregateFunction(list)]);
                    list = [];
                }
            }
        }
        return outputArray;
    }
    /**
     * Decodes the function macro into a function
     * @param {*} code
     */
    _decodeAggregate(code) {
        if (code === Timeseries.AGG_SUM)
            return utils_js_1.default._sum;
        if (code === Timeseries.AGG_MAX)
            return utils_js_1.default._max;
        if (code === Timeseries.AGG_MIN)
            return utils_js_1.default._min;
        if (code === Timeseries.AGG_MEAN)
            return utils_js_1.default._mean;
        if (code === Timeseries.AGG_VAR)
            return utils_js_1.default._variance;
        if (code === Timeseries.AGG_STDDEV)
            return utils_js_1.default._standardDeviation;
        if (code === Timeseries.AGG_MEDIAN)
            return utils_js_1.default._median;
        if (code === Timeseries.AGG_COUNT)
            return utils_js_1.default._count;
    }
    /**
     * Helper function for testing
     */
    _twoDData() {
        let i = this.start;
        let output = [];
        let hh = 0;
        this._rawdata.forEach(element => {
            hh = luxon_1.DateTime.fromMillis(i).hour * 2;
            if (luxon_1.DateTime.fromMillis(i).minute > 29)
                hh++;
            i += this.interval;
            output.push([i, hh, element]);
        });
        return output;
    }
}
exports.default = Timeseries;
