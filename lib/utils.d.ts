declare namespace _default {
    export { isArray };
    export { isArrayBuffer };
    export { isBuffer };
    export { isFormData };
    export { isArrayBufferView };
    export { isString };
    export { isNumber };
    export { isObject };
    export { isPlainObject };
    export { isUndefined };
    export { isDate };
    export { isFile };
    export { isBlob };
    export { isFunction };
    export { isStream };
    export { isURLSearchParams };
    export { isStandardBrowserEnv };
    export { forEach };
    export { merge };
    export { extend };
    export { trim };
    export { stripBOM };
    export { _assertNotEmpty };
    export { _assertSameLength };
    export { _decodeAggregate };
    export { _sum };
    export { _mean };
    export { _median };
    export { _variance };
    export { _standardDeviation };
    export { _covariance };
    export { _skewness };
    export { _leastSquaresFit };
    export { _min };
    export { _max };
    export { _nzmin };
    export { _histogram };
    export { _count };
}
export default _default;
/**
 * Determine if a value is an Array
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Array, otherwise false
 */
declare function isArray(val: Object): boolean;
/**
 * Determine if a value is an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an ArrayBuffer, otherwise false
 */
declare function isArrayBuffer(val: Object): boolean;
/**
 * Determine if a value is a Buffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Buffer, otherwise false
 */
declare function isBuffer(val: Object): boolean;
/**
 * Determine if a value is a FormData
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an FormData, otherwise false
 */
declare function isFormData(val: Object): boolean;
/**
 * Determine if a value is a view on an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
 */
declare function isArrayBufferView(val: Object): boolean;
/**
 * Determine if a value is a String
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a String, otherwise false
 */
declare function isString(val: Object): boolean;
/**
 * Determine if a value is a Number
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Number, otherwise false
 */
declare function isNumber(val: Object): boolean;
/**
 * Determine if a value is an Object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Object, otherwise false
 */
declare function isObject(val: Object): boolean;
/**
 * Determine if a value is a plain Object
 *
 * @param {Object} val The value to test
 * @return {boolean} True if value is a plain Object, otherwise false
 */
declare function isPlainObject(val: Object): boolean;
/**
 * Determine if a value is undefined
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if the value is undefined, otherwise false
 */
declare function isUndefined(val: Object): boolean;
/**
 * Determine if a value is a Date
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Date, otherwise false
 */
declare function isDate(val: Object): boolean;
/**
 * Determine if a value is a File
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a File, otherwise false
 */
declare function isFile(val: Object): boolean;
/**
 * Determine if a value is a Blob
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Blob, otherwise false
 */
declare function isBlob(val: Object): boolean;
/**
 * Determine if a value is a Function
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Function, otherwise false
 */
declare function isFunction(val: Object): boolean;
/**
 * Determine if a value is a Stream
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Stream, otherwise false
 */
declare function isStream(val: Object): boolean;
/**
 * Determine if a value is a URLSearchParams object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a URLSearchParams object, otherwise false
 */
declare function isURLSearchParams(val: Object): boolean;
/**
 * Determine if we're running in a standard browser environment
 *
 * This allows axios to run in a web worker, and react-native.
 * Both environments support XMLHttpRequest, but not fully standard globals.
 *
 * web workers:
 *  typeof window -> undefined
 *  typeof document -> undefined
 *
 * react-native:
 *  navigator.product -> 'ReactNative'
 * nativescript
 *  navigator.product -> 'NativeScript' or 'NS'
 */
declare function isStandardBrowserEnv(): boolean;
/**
 * Iterate over an Array or an Object invoking a function for each item.
 *
 * If `obj` is an Array callback will be called passing
 * the value, index, and complete array for each item.
 *
 * If 'obj' is an Object callback will be called passing
 * the value, key, and complete object for each property.
 *
 * @param {Object|Array} obj The object to iterate
 * @param {Function} fn The callback to invoke for each item
 */
declare function forEach(obj: Object | any[], fn: Function): void;
/**
 * Accepts varargs expecting each argument to be an object, then
 * immutably merges the properties of each object and returns result.
 *
 * When multiple objects contain the same key the later object in
 * the arguments list will take precedence.
 *
 * Example:
 *
 * ```js
 * var result = merge({foo: 123}, {foo: 456});
 * console.log(result.foo); // outputs 456
 * ```
 *
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */
declare function merge(...args: any[]): Object;
/**
 * Extends object a by mutably adding to it the properties of object b.
 *
 * @param {Object} a The object to be extended
 * @param {Object} b The object to copy properties from
 * @param {Object} thisArg The object to bind function to
 * @return {Object} The resulting value of object a
 */
declare function extend(a: Object, b: Object, thisArg: Object): Object;
/**
 * Trim excess whitespace off the beginning and end of a string
 *
 * @param {String} str The String to trim
 * @returns {String} The String freed of excess whitespace
 */
declare function trim(str: string): string;
/**
 * Remove byte order marker. This catches EF BB BF (the UTF-8 BOM)
 *
 * @param {string} content with BOM
 * @return {string} content value without BOM
 */
declare function stripBOM(content: string): string;
declare function _assertNotEmpty(list: any): void;
declare function _assertSameLength(list1: any, list2: any): void;
declare function _decodeAggregate(code: any): typeof _median | undefined;
declare function _sum(list: any): number;
declare function _mean(list: any): number;
declare function _median(list: any): any;
declare function _variance(list: any): number;
declare function _standardDeviation(list: any): number;
declare function _covariance(xcol: any, ycol: any): number;
declare function _skewness(list: any): number;
declare function _leastSquaresFit(xcol: any, ycol: any): number[];
declare function _min(list: any): number;
declare function _max(list: any): number;
declare function _nzmin(list: any): number;
declare function _histogram(list: any): number[];
declare function _count(list: any): any;
