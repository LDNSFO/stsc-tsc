import { CarbonIntensity } from '../index';
import { Carbon } from '../index';
import { Energy } from '../index';

let mycarbonintensity = new CarbonIntensity();
let mycarbon = new Carbon();
let myenergy = new Energy();

test('Empty carbon object', () => {
  expect(mycarbonintensity.sum()).toBe(0);
});

test('Full carbon object', async () => {
  await mycarbonintensity.load();
  //console.log(mycarbonintensity);

  mycarbon.carbonintensity = mycarbonintensity;
  //console.log(mycarbon);

  myenergy._token = 'resource_test';
  await myenergy.load();
  mycarbon.energy = myenergy;
  //console.log(mycarbon);

  mycarbon.calc();

  //console.log(mycarbon);
});
