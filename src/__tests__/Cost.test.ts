import { Energy } from '../';
import { Tariff } from '../';
import { Cost } from '../';

import { DateTime } from 'luxon';

let myenergy = new Energy();
let mytariff = new Tariff();
let mycost = new Cost();

test('Test basic cost', async () => {
  myenergy._token = 'resource_test';
  await myenergy.load();
  mytariff.from = '2020-01-01T00:00:00Z0';
  mytariff.standingcharge = 26;
  mytariff.flatPrice = { rate: 9.5 };

  mycost.energy = myenergy;
  mycost.tariff = mytariff;

  // Spreadsheet says £774.08
  // stsc-tsc says £ 774.081885

  // console.log(mycost.annual);
  // console.log(mycost);
});

test('Test tou cost', async () => {
  myenergy._token = 'resource_test';
  await myenergy.load();
  mytariff.touPrice = [
    { valid_from: '2021-03-02T00:00:00.000+00:00', valid_to: '2021-03-02T06:00:00.000+00:00', value_inc_vat: 9.5 },
    { valid_from: '2021-03-02T06:00:00.000+00:00', valid_to: '2021-03-02T16:30:00.000+00:00', value_inc_vat: 14.2 },
    { valid_from: '2021-03-02T16:30:00.000+00:00', valid_to: '2021-03-03T00:00:00.000+00:00', value_inc_vat: 24.7 },
  ];
  mytariff.standingcharge = 0.3;

  mycost.energy = myenergy;
  mycost.tariff = mytariff;

  // Spreadsheet says £ 1332.74
  // stsc-tsc says £ 1329.252619

  // console.log(mycost.annual);
  // console.log(mycost);
});

// test('Test tou cost', async () => {
//   myenergy._token = 'resource_test';
//   await myenergy.load();
//   mytariff.loadDynamicPrice('');
//   mytariff.standingcharge = 26;

//   mycost.energy = myenergy;
//   mycost.tariff = mytariff;

//   // Spreadsheet says £ 916.51
//   // stsc-tsc says £

//   console.log(mycost.annual);
//   console.log(mycost);
// });
