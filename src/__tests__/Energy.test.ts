import { Energy } from '../index';
import { Timeseries } from '..';
import { DateTime } from 'luxon';

test('Empty energy object', () => {
  let myenergy = new Energy();
  expect(myenergy.sum()).toBe(0);
});

test('Full energy object', async () => {
  let myenergy = new Energy();
  myenergy._token = 'resource_test';
  await myenergy.load();
  expect(myenergy.count()).toBe(28800);
});

test('Properties of energy object', async () => {
  let myenergy = new Energy();
  myenergy._token = 'resource_test';
  await myenergy.load();
  expect(myenergy.start).toBe(1565395200000);
  expect(myenergy.end).toBe(1617233400000);
  expect(myenergy.interval).toBe(1800000);

  expect(myenergy.count()).toBe(28800);
  expect(myenergy.nzmin()).toBe(1);
  expect(myenergy.max()).toBe(3515);
  expect(myenergy.mean()).toBe(358.19086805555554);
  expect(myenergy.median()).toBe(242);
  expect(myenergy.min()).toBe(0);
  expect(myenergy.stddev()).toBe(399.8554813494741);
  expect(myenergy.sum()).toBe(10315897);
  expect(myenergy.variance()).toBe(159884.40596521963);
});

test('Energy export check', async () => {
  let myenergy = new Energy();
  myenergy._token = 'resource_test';
  await myenergy.load();
  let exportObj = myenergy.export();
  expect(exportObj[0].ts).toBe(1565395200000);
  expect(exportObj[0].value).toBe(0);
  expect(exportObj[28799].ts).toBe(1565395200000 + 1800000 * 28799);
  expect(exportObj[28799].value).toBe(251);
});

test('Energy byMonth', async () => {
  let myenergy = new Energy();
  myenergy._token = 'resource_test';
  await myenergy.load();

  let monthssummary = myenergy.byMonth();

  expect(monthssummary[0][0]).toBe(1585699200000);
  expect(monthssummary[0][1]).toBe(683979);
  expect(monthssummary[1][0]).toBe(1588291200000);
  expect(monthssummary[1][1]).toBe(614257);
  expect(monthssummary[2][0]).toBe(1590969600000);
  expect(monthssummary[2][1]).toBe(616055);
  expect(monthssummary[3][0]).toBe(1593561600000);
  expect(monthssummary[3][1]).toBe(621205);
  expect(monthssummary[4][0]).toBe(1596240000000);
  expect(monthssummary[4][1]).toBe(561564);
  expect(monthssummary[5][0]).toBe(1598918400000);
  expect(monthssummary[5][1]).toBe(525922);
  expect(monthssummary[6][0]).toBe(1601510400000);
  expect(monthssummary[6][1]).toBe(550838);
  expect(monthssummary[7][0]).toBe(1604188800000);
  expect(monthssummary[7][1]).toBe(555873);
  expect(monthssummary[8][0]).toBe(1606780800000);
  expect(monthssummary[8][1]).toBe(675934);
  expect(monthssummary[9][0]).toBe(1609459200000);
  expect(monthssummary[9][1]).toBe(582980);
  expect(monthssummary[10][0]).toBe(1612137600000);
  expect(monthssummary[10][1]).toBe(562096);
  expect(monthssummary[11][0]).toBe(1614556800000);
  expect(monthssummary[11][1]).toBe(598580);
});

test('Energy profile', async () => {
  let localenergy = new Energy();
  localenergy._token = 'resource_test';
  await localenergy.load();

  expect(localenergy._modified > localenergy._cacheModified).toBe(true);
  let profile = localenergy.profile();

  expect(profile.nonzeromin).toBe(1);
  expect(profile.max).toBe(3515);
  expect(profile.mean).toBe(358.19086805555554);
  expect(profile.min).toBe(0);
  expect(profile.stddev).toBe(399.8554813494734);

  expect(localenergy._modified > localenergy._cacheModified).toBe(false);
  let secondprofile = localenergy.profile();

  localenergy.scale(1);
  expect(localenergy._modified >= localenergy._cacheModified).toBe(true);
  let thirdprofile = localenergy.profile();

  expect(profile.nonzeromin).toBe(1);
  expect(profile.max).toBe(3515);
  expect(profile.mean).toBe(358.19086805555554);
  expect(profile.min).toBe(0);
  expect(profile.stddev).toBe(399.8554813494734);
});

test('byMonth', async () => {
  let myenergy = new Energy();
  myenergy._token = 'resource_test';
  await myenergy.load();

  // console.log(myenergy.byMonth({ aggregateFunction: Timeseries.AGG_SUM }));
});

test('Energy slot data', async () => {
  let myenergy = new Energy();
  myenergy._token = 'resource_test';
  await myenergy.load();

  let slotdata = myenergy.slotdata;
  expect(slotdata[0]).toBe(0);
  expect(slotdata[28799]).toBe(251);
});

test('Add energy - simple check', async () => {
  let myenergy = new Energy();
  myenergy._token = 'resource_test';
  await myenergy.load();

  let newenergy = myenergy.addEnergy({
    start: 0,
    end: 12,
    amount: 1000,
  });

  let profile = newenergy.profile();
  expect(profile.mean).toBe(521.6978125);
});

test('byDOW - by day of week', async () => {
  let myenergy = new Energy();
  myenergy._token = 'resource_test';
  await myenergy.load();

  // console.log(myenergy.byDOW({ aggregateFunction: Timeseries.AGG_COUNT }));
  // console.log(myenergy.byDOW({ aggregateFunction: Timeseries.AGG_MAX }));
  // console.log(myenergy.byDOW({ aggregateFunction: Timeseries.AGG_MIN }));
  // console.log(myenergy.byDOW({ aggregateFunction: Timeseries.AGG_MEDIAN }));
  // console.log(myenergy.byDOW({ aggregateFunction: Timeseries.AGG_MEAN }));
  // console.log(myenergy.byDOW({ aggregateFunction: Timeseries.AGG_SUM }));
});

test('byHHSlot - by half hourly slot', async () => {
  let myenergy = new Energy();
  myenergy._token = 'resource_test';
  await myenergy.load();

  // console.log(myenergy.byHHSlot({ aggregateFunction: Timeseries.AGG_COUNT }));
  // console.log(myenergy.byHHSlot({ aggregateFunction: Timeseries.AGG_MAX }));
  // console.log(myenergy.byHHSlot({ aggregateFunction: Timeseries.AGG_MIN }));
  // console.log(myenergy.byHHSlot({ aggregateFunction: Timeseries.AGG_MEDIAN }));
  // console.log(myenergy.byHHSlot({ aggregateFunction: Timeseries.AGG_MEAN }));
  // console.log(myenergy.byHHSlot({ aggregateFunction: Timeseries.AGG_SUM }));
});

test('byHHSlotDOW - by half hourly slot day of week 7x48', async () => {
  let myenergy = new Energy();
  myenergy._token = 'resource_test';
  await myenergy.load();

  // console.log(myenergy.byHHSlotDOW({ aggregateFunction: Timeseries.AGG_COUNT }));
  // console.log(myenergy.byHHSlotDOW({ aggregateFunction: Timeseries.AGG_MAX }));
  // console.log(myenergy.byHHSlotDOW({ aggregateFunction: Timeseries.AGG_MIN }));
  // console.log(myenergy.byHHSlotDOW({ aggregateFunction: Timeseries.AGG_MEDIAN }));
  // console.log(myenergy.byHHSlotDOW({ aggregateFunction: Timeseries.AGG_MEAN }));
  // console.log(myenergy.byHHSlotDOW({ aggregateFunction: Timeseries.AGG_SUM }));
});

test('byHHSlotLocaltime - by half hourly slot day of week 7x48', async () => {
  let myenergy = new Energy();
  myenergy._token = 'resource_test';
  await myenergy.load();

  expect(myenergy.byHHSlotLocaltime({ aggregateFunction: Timeseries.AGG_COUNT })[0]).toBe(600);
  expect(myenergy.byHHSlotLocaltime({ aggregateFunction: Timeseries.AGG_COUNT })[1]).toBe(600);
  expect(myenergy.byHHSlotLocaltime({ aggregateFunction: Timeseries.AGG_COUNT })[2]).toBe(600);

  // console.log(myenergy.byHHSlotLocaltime({ aggregateFunction: Timeseries.AGG_COUNT }));
  // console.log(myenergy.byHHSlotLocaltime({ aggregateFunction: Timeseries.AGG_MAX }));
  // console.log(myenergy.byHHSlotLocaltime({ aggregateFunction: Timeseries.AGG_MIN }));
  // console.log(myenergy.byHHSlotLocaltime({ aggregateFunction: Timeseries.AGG_MEDIAN }));
  // console.log(myenergy.byHHSlotLocaltime({ aggregateFunction: Timeseries.AGG_MEAN }));
  // console.log(myenergy.byHHSlotLocaltime({ aggregateFunction: Timeseries.AGG_SUM }));
});

test('scale - multiply by 10, mirror export', async () => {
  let myenergy = new Energy();
  myenergy._token = 'resource_test';
  await myenergy.load();

  myenergy.scale(10);
  expect(myenergy.sum()).toBe(103158970);

  myenergy.scale(0.1);
  expect(myenergy.sum()).toBe(10315897);

  myenergy.scale(-1);
  expect(myenergy.sum()).toBe(-10315897);
});

test('translate - add 10, subtract 10 from each point', async () => {
  let myenergy = new Energy();
  myenergy._token = 'resource_test';
  await myenergy.load();

  myenergy.translate(10);
  expect(myenergy.sum()).toBe(10315897 + 288000);

  myenergy.translate(-10);
  expect(myenergy.sum()).toBe(10315897);

  myenergy.translate(100);
  expect(myenergy.sum()).toBe(10315897 + 2880000);
});

test('!!!!! consumption - all of these show problem with P1M luxon !!!!!', async () => {
  let myenergy = new Energy();
  myenergy._token = 'resource_test';
  await myenergy.load();

  // console.log('Start time of energy: ' + DateTime.fromMillis(myenergy.start).toISO());
  // console.log('End time of energy: ' + DateTime.fromMillis(myenergy.end).toISO());

  let params = {
    from: DateTime.fromMillis(myenergy.start).toISO(),
    to: DateTime.fromMillis(myenergy.end).toISO(),
    intervalISO: 'P1M',
    aggregateFunction: Timeseries.AGG_SUM,
  };
  let consumption = myenergy.consumption(params);
  // console.log(consumption);

  params = {
    from: DateTime.fromMillis(myenergy.start).toISO(),
    to: DateTime.fromMillis(myenergy.end).toISO(),
    intervalISO: 'P1M',
    aggregateFunction: Timeseries.AGG_COUNT,
  };
  consumption = myenergy.consumption(params);
  // console.log(consumption);

  params = {
    from: DateTime.fromMillis(myenergy.start).toISO(),
    to: DateTime.fromMillis(myenergy.end).toISO(),
    intervalISO: 'P1M',
    aggregateFunction: Timeseries.AGG_MEAN,
  };
  consumption = myenergy.consumption(params);
  // console.log(consumption);
});

test('range - use some general parameters', async () => {
  let myenergy = new Energy();
  myenergy._token = 'resource_test';
  await myenergy.load();
  let newenergy = myenergy.range({
    from: '2020-02-01T00:00:00.000+00:00',
    to: '2021-02-01T00:00:00.000+00:00',
    interval: 1800000,
  });
  // console.log(newenergy);

  // this will equal the 00:00 of the day given
  expect(newenergy.start).toBe(1580515200000);

  // this will be the last 30 min of the day before
  expect(newenergy.end).toBe(1612135800000);

  // known what the first entry is, this stays the same as we are hard setting the time range
  expect(newenergy._rawdata[0]).toBe(157);

  // known what the last entry is, this stays the same as we are hard setting the time range
  let lastentryIndex = (1612135800000 - 1580515200000) / 1800000;
  // console.log(lastentryIndex);
  expect(newenergy._rawdata[lastentryIndex]).toBe(164);

  // known what the sum is, this stays the same as we are hard setting the time range
  expect(newenergy.sum()).toBe(7162176);
});
