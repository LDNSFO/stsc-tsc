import { Energy } from '../index';
import { Timeseries } from '..';
import { DateTime } from 'luxon';

let token =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbkhhc2giOiIzZmU3N2U2ZjhiMzliYzBiOWU1ZGNmOGQ2ODNmMmE4MjQ2YTlkMzg2MzJlN2ZiZDAzOWM2YjhhZWMzZGY5MGExNjliOTVmYjA4ODk2M2JiNmNjZjdjODViZWI1OTVlZTgiLCJpYXQiOjE2MTg2NzA0OTksImV4cCI6MTYxOTI3NTI5OX0.VS4wotz0M2C0ufgeYshqnBprWk94o_708mquKDijj1A';

test('Empty energy object', () => {
  let myenergy = new Energy();
  expect(myenergy.sum()).toBe(0);
});

test('Gas energy object', async () => {
  let myenergy = new Energy();
  myenergy._token = token;
  myenergy.fuel = 'gas';
  await myenergy.load();

  console.log(myenergy);
});
