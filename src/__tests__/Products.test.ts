import { Products, Energy } from '../index';
import { Suppliers } from '../index';
import { Tariff } from '../index';

test('Load Products', async () => {
  let mysuppliers = new Suppliers();
  let myproducts = new Products();

  await mysuppliers.load();
  myproducts.setSuppliers(mysuppliers);

  await myproducts.load();
  myproducts.gsp = 'A';
  await myproducts.loadtariffs();

  //   console.log(myproducts);

  expect(myproducts.products.length).toBe(47);
});

test('Integrated test', async () => {
  //   await myproducts.basicCalcTest();
  let energy = new Energy();
  energy._token = 'resource_test';
  await energy.load();

  let myproducts = new Products();

  let mysuppliers = new Suppliers();
  await mysuppliers.load();
  myproducts.setSuppliers(mysuppliers);

  myproducts.gsp = 'A';
  await myproducts.load();
  await myproducts.loadtariffs();

  myproducts.predictedEnergy = energy;
  myproducts.calcCosts();

  // console.log(myproducts);

  expect(myproducts.products.length).toBe(47);
});
