import { STSC, Suppliers, Products, Energy } from '../index';

test('Login', async () => {
  let mystsc = new STSC();
  await mystsc.login({ username: 'smartenergydemo@glowtest.com', password: 'T3stPassw0rd!' });
  await mystsc.loadgsp();
  // console.log(mystsc);
});

test('Load GSP', async () => {
  let mystsc = new STSC();
  mystsc.token = 'resource_test';
  await mystsc.loadgsp();
  expect(mystsc.gsp).toBe('J');
});

test('Load suppliers and products', async () => {
  let mystsc = new STSC();
  await mystsc.login({ username: 'smartenergydemo@glowtest.com', password: 'T3stPassw0rd!' });
  await mystsc.loadgsp();

  let mysuppliers = new Suppliers();
  mysuppliers.token = mystsc.token;
  await mysuppliers.load();
  mystsc.setSuppliers(mysuppliers);

  let myproducts = new Products();
  myproducts.token = mystsc.token;
  myproducts.gsp = mystsc.gsp;
  myproducts.setSuppliers(mysuppliers);

  let myenergy = new Energy();
  myenergy._token = 'resource_test';
  await myenergy.load();
  myproducts.predictedEnergy = myenergy;

  await myproducts.load();
  await myproducts.loadtariffs();

  myproducts.calcCosts();

  // mystsc.setProducts(myproducts);
});
