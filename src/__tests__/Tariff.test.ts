import { Tariff } from '../index';

let mytariff = new Tariff();

test('Empty dynamic tariff object', () => {
  expect(mytariff.sum()).toBe(0);
});

test('Set up a flat rate tariff', () => {
  mytariff.flatPrice = { rate: 0.095 };
  mytariff.from = '2020-01-01T00:00:00Z';
  mytariff.standingcharge = 0.26;
  // console.log(mytariff);
  expect(mytariff.standingcharge).toBe(0.26);
  expect(mytariff._flat.rate).toBe(0.095);
  expect(mytariff.type).toBe('flat');
});

let toutariff = new Tariff();

test('Set up a TOU rate tariff', () => {
  toutariff.touPrice = [
    { valid_from: '2021-03-02T00:00:00.000+00:00', valid_to: '2021-03-02T06:00:00.000+00:00', value_inc_vat: 0.095 },
    { valid_from: '2021-03-02T06:00:00.000+00:00', valid_to: '2021-03-02T16:30:00.000+00:00', value_inc_vat: 0.142 },
    { valid_from: '2021-03-02T16:30:00.000+00:00', valid_to: '2021-03-03T00:00:00.000+00:00', value_inc_vat: 0.247 },
  ];
  toutariff.standingcharge = 0.3;
  expect(toutariff.standingcharge).toBe(0.3);
  expect(toutariff._tou[0]).toBe(0.095);
  expect(toutariff._tou[13]).toBe(0.142);
  expect(toutariff._tou[47]).toBe(0.247);
  expect(toutariff.type).toBe('tou');
});

test('Set up a dynamic rate tariff', async () => {
  let mydyntariff = new Tariff();
  await mydyntariff.loadDynamicPrice({ name: 'AGILE-18-02-21', gsp: 'J' });
  expect(mydyntariff.type).toBe('dynamic');
});

test('Set up a flat rate tariff from a tariff', async () => {
  var sampletariff = {
    id: 43,
    fkid: 'LP-FIX-12M-20-11-26',
    displayname: 'Tomato fixed yearly plan',
    name: 'Tomato fixed yearly plan 12m fixed november 2020 v1',
    description:
      "This plan fixes your unit rates and standing charges for 12 months. There are no exit fees, so if you change your mind, you're in control. This plan features 100% renewable electricity.",
    is_variable: false,
    is_green: false,
    is_prepay: false,
    available_from: '2020-11-26T00:00:00Z',
    available_to: null,
    term: 12,
    brand: 'TOMATO',
    type: 'flat',
    paymenttype: 'dd',
    standingcharge: 19.53,
    rate: { unitrate: 15.4035 },
    gsp: 'J',
  };

  let myflattariff = new Tariff();
  await myflattariff.setTariff(sampletariff);
  expect(myflattariff.type).toBe('flat');
});

test('Set up a dynamic rate tariff from a tariff', async () => {
  var sampletariff = {
    id: 2,
    fkid: 'AGILE-18-02-21',
    displayname: 'Dynamo Pear',
    name: 'Dynamo Pear February 2018',
    description: '',
    is_variable: true,
    is_green: true,
    is_prepay: false,
    available_from: '2017-01-01T00:00:00Z',
    available_to: null,
    term: 12,
    brand: 'PEAR_ENERGY',
    type: 'dynamic',
    paymenttype: 'dd',
    standingcharge: 21,
    gsp: 'J',
  };

  let mydyntariff = new Tariff();
  await mydyntariff.setTariff(sampletariff);
  expect(mydyntariff.type).toBe('dynamic');
});

test('Set up a TOU rate tariff from a tariff', async () => {
  var sampletariff = {
    id: 37,
    fkid: 'GO-5H-2230',
    displayname: 'Pear EV Faster',
    name: 'Pear EV Faster (5H from 2230) December 2019 v1',
    description: 'Pear EV Faster (5H from 2230)',
    is_variable: true,
    is_green: true,
    is_prepay: false,
    available_from: '2019-12-10T00:00:00Z',
    available_to: null,
    term: 12,
    brand: 'PEAR_ENERGY',
    type: 'tou',
    paymenttype: 'dd',
    standingcharge: 25.0005,
    rate: [
      {
        value_exc_vat: 5.238,
        value_inc_vat: 5.4999,
        valid_from: '2021-01-26T22:30:00Z',
        valid_to: '2021-01-27T03:30:00Z',
      },
      {
        value_exc_vat: 13.71,
        value_inc_vat: 14.3955,
        valid_from: '2021-01-26T03:30:00Z',
        valid_to: '2021-01-26T22:30:00Z',
      },
    ],
    gsp: 'J',
  };

  let mytoutariff = new Tariff();
  await mytoutariff.setTariff(sampletariff);
  expect(mytoutariff.type).toBe('tou');
});
