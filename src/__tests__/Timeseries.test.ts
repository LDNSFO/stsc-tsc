import { Timeseries } from '../index';

/**
 * Basic tests of the time series and functions
 */
let myts = new Timeseries();

test('Assign data to timeseries', () => {
  myts.data = [
    [1577836800000, 0],
    [1580515200000, 400],
    [1583020800000, 500],
    [1585699200000, 600],
  ];

  // Test initialisation of cache
  expect(myts._modified > myts._cacheModified).toBe(true);

  expect(myts.start).toBe(1577836800000);
  expect(myts.end).toBe(1585699200000);
  expect(myts.count()).toBe(4);
  expect(myts.nzmin()).toBe(400);
  expect(myts.max()).toBe(600);
  expect(myts.mean()).toBe(375);
  expect(myts.median()).toBe(450);
  expect(myts.min()).toBe(0);
  expect(myts.stddev()).toBe(227.76083947860747);
  expect(myts.sum()).toBe(1500);
  expect(myts.variance()).toBe(51875);

  // Test cache
  myts.scale(1);

  expect(myts._cacheModified <= myts._modified).toBe(true);

  expect(myts.start).toBe(1577836800000);
  expect(myts.end).toBe(1585699200000);
  expect(myts.count()).toBe(4);
  expect(myts.nzmin()).toBe(400);
  expect(myts.max()).toBe(600);
  expect(myts.mean()).toBe(375);
  expect(myts.median()).toBe(450);
  expect(myts.min()).toBe(0);
  expect(myts.stddev()).toBe(227.76083947860747);
  expect(myts.sum()).toBe(1500);
  expect(myts.variance()).toBe(51875);

  expect(myts._modified <= myts._cacheModified).toBe(true);
});

/**
 * Assignment and Range functions
 */

test('Assign more data to timeseries', () => {
  let biggerts = new Timeseries();

  // number of 30 minute time slots to populate
  let j = 48 * 600;
  let end = 1617235200000;
  let interval = 1800000;
  let start = end - j * interval;
  let data = [];

  // time order is important -- low to high timestamp
  for (let i = 0; i < j; i++) {
    data.push([start + interval * i, 1]);
  }

  biggerts.data = data;

  // Test initialisation of cache
  expect(biggerts._modified > biggerts._cacheModified).toBe(true);

  expect(biggerts.count()).toBe(28800);
  expect(biggerts.sum()).toBe(28800);

  expect(biggerts._modified <= biggerts._cacheModified).toBe(true);

  // let rangets = biggerts.range({
  //   start: 1612137600000,
  //   end: 1614556800000,
  //   interval: 1800000
  // })

  let rangets = biggerts.range({
    from: '2021-02-01T00:00:00.000Z',
    to: '2021-03-01T00:00:00.000Z',
    intervalISO: 'PT30M',
  });

  expect(rangets.count()).toBe(1344);

  let leapyearts = biggerts.range({
    from: '2020-02-01T00:00:00.000Z',
    to: '2020-03-01T00:00:00.000Z',
    intervalISO: 'PT30M',
  });

  expect(leapyearts.count()).toBe(1392);

  let startendts = biggerts.range({
    start: 1580515200000,
    end: 1583020800000,
    interval: 1800000,
  });

  expect(startendts.count()).toBe(1392);

  let weeks = biggerts.range({
    from: '2020-01-01T00:00:00.000Z',
    to: '2020-03-01T00:00:00.000Z',
    intervalISO: 'P1W',
  });

  // This should be 9? so a fail
  expect(weeks.count()).toBe(8);
  // console.log(weeks);
});

/**
 * Summary functions
 */

test('Half hourly', () => {
  let bigts = new Timeseries();

  // number of 30 minute time slots to populate
  let j = 48 * 100;
  let end = 1617235200000;
  let interval = 1800000;
  let start = end - j * interval;
  let data = [];

  // time order is important -- low to high timestamp
  for (let i = start; i <= end; i += interval) {
    data.push([i, 1]);
  }

  bigts.data = data;
  // console.log(bigts);
  // console.log('From : ' + bigts.from + ' - To: ' + bigts.to);
  // console.log(bigts.byHHSlot({}));
  // console.log(bigts.byHHSlotLocaltime({}));
});

test('Half hourly DOW', () => {
  let bigts = new Timeseries();

  // number of 30 minute time slots to populate
  let j = 48 * 100;
  let end = 1617235200000;
  let interval = 1800000;
  let start = end - j * interval;
  let data = [];

  // time order is important -- low to high timestamp
  for (let i = start; i <= end; i += interval) {
    data.push([i, 1]);
  }

  bigts.data = data;

  // console.log(bigts.byHHSlotDOW({}));
  // console.log(bigts.byHHSlotDOWLocaltime({}));
});
