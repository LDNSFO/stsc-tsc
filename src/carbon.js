'use strict';

/**
 * Carbon
 * Application class that takes an Energy Object and a CarbonIntensity object
 * to calculation the carbon used during consumption period
 * 
 * Copyright 2020 - 2021 Hildebrand Technology Limit
 * Author: Joshua Cooper - jcooper@hildebrand.co.uk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy o
 * this software and associated documentation files (the "Software"), to deal in th
 * Software without restriction, including without limitation the rights to use
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copie
 * of the Software, and to permit persons to whom the Software is furnishe
 * to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import * as mTimeseries from './timeseries.js';
import * as mEnergy from './energy.js';
import * as mCarbonIntensity from './carbonintensity.js';

const Timeseries = mTimeseries.default || mTimeseries;
const CarbonIntensity = mCarbonIntensity.default || mCarbonIntensity;
const Energy = mEnergy.default || mEnergy;

export default class Carbon extends Timeseries {

    constructor() {
        super();
        this._token = '';
        this._type = 'CarbonTimeseries';
        this.energy = new Energy();
        this.carbonintensity = new CarbonIntensity();
    }

    calc() {

        this.start = this.energy.start;
        this.end = this.energy.end;
        this.interval = this.energy.interval;

        let incrementSlot = 0;
        let carbonSlot = parseInt((this.energy.start - this.carbonintensity.start) / this.energy.interval, 10);
        if(carbonSlot > 0) {

            this.energy._rawdata.forEach((consumption) => {
                this._rawdata[incrementSlot] = this.carbonintensity._rawdata[carbonSlot] * consumption;
                carbonSlot++;
                incrementSlot++;
            });
            
        }

    }


}