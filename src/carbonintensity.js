'use strict';

/**
 * CarbonIntensity
 * Class for loading 30 minute carbon intensity from National Grid
 * 
 * Copyright 2020 - 2021 Hildebrand Technology Limit
 * Author: Joshua Cooper - jcooper@hildebrand.co.uk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy o
 * this software and associated documentation files (the "Software"), to deal in th
 * Software without restriction, including without limitation the rights to use
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copie
 * of the Software, and to permit persons to whom the Software is furnishe
 * to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import axios from 'axios';
import * as mTimeseries from './timeseries.js';

const Timeseries = mTimeseries.default || mTimeseries;

export default class CarbonIntensity extends Timeseries {

    constructor() {
        super();
        this._token = '';
        this.gsp = 'A';
        this._type = 0x0A;
    }

    _intervalDecode(intervalCode) {
        if(intervalCode === 0x0F) return (30 * 60 * 1000);
    }


    async load() {
        // web call to gsp
        // then load into structure
        let stscServiceURL = 'https://beta.smarttariffsmartcomparison.org/ci.php';
        stscServiceURL += '?gsp=' + this.gsp;

        await axios(stscServiceURL, { 
            responseType: 'arraybuffer',
            headers: { 
                //'Cache-Control': 'no-store',
                'token': this._token
            } 
        }).then((response) => {

            if(response.status === 200) {
                this._modified = response.headers['last-modified'];

                let ui8 = new Uint8Array(response.data).buffer;
                const view = new DataView(ui8);

                this._type = view.getUint8(0);
                this._rawinterval = view.getUint8(1);
                this.start = view.getUint32(2)*1000;
                
                // get the size
                //const numOfElements = parseInt(response.headers['content-length'], 10) - 6;
                const numOfElements = parseInt(ui8.byteLength, 10) - 6;
                this._rawdata = new Float64Array(parseInt(numOfElements/2, 10));
                let j = 0;
                for(let i=0; i < numOfElements; i+=2) {
                    this._rawdata[j] = view.getUint16(i+6);
                    j++;
                }

                this.interval = this._intervalDecode(this._rawinterval); // seconds
                this.end = this.start + (this.interval * (j-1));

            }
        })
        .catch((error) => {
                throw new Error(error);
        });
    }

}