/**
 * Cost
 * Application class that returns a timeseries that is a calculation
 * based on energy and tariff objects attached to it.
 * 
 * Copyright 2020 - 2021 Hildebrand Technology Limit
 * Author: Joshua Cooper - jcooper@hildebrand.co.uk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy o
 * this software and associated documentation files (the "Software"), to deal in th
 * Software without restriction, including without limitation the rights to use
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copie
 * of the Software, and to permit persons to whom the Software is furnishe
 * to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import { DateTime, Duration, Interval, Info } from "luxon";
import * as mTimeseries from './timeseries.js';
import * as mTariff from './tariff.js';
import * as mEnergy from './energy';

const Timeseries = mTimeseries.default || mTimeseries;
const Energy = mEnergy.default || mEnergy;
const Tariff = mTariff.default || mTariff;

export default class Cost extends Timeseries {

    constructor() {
        super();
        this.energy = new Energy();
        this.tariff = new Tariff();

        /**
         * pence per W to pounds per kW (or any other decimel money)
         */ 
        this._units_multiplier = 0.001;
        this._monetary_multiplier = 0.01;
        this._vat = 0.05;

        this._annual = 0;
        this._byMonth = [];

        this._cacheModified = 0;

        // this.tariff = tariff;
    }

    /**
     * Rollup costs as annual, use the byMonth method
     * Note: indexing is last 12 months, not Jan - Dec
     */
    get annual() {
        let cost = 0;
        
        this.byMonth().forEach((month) => {
            cost += month[1];
        });

        return cost;
    }

    /**
     * byMonth - aggregate costs by month
     * Frequently monthly costs are interesting, this is a property
     * implemented as a function of the Cost class
     * last 12 months versus calendar year
     */
    
    byMonth(param) {
        let months = [];
        let intervalsDateTime = [];
        let sc=0;
        let cc=0;
        let consumption=0;

        // if we want to have an arbitrary from / to date then this is a way to do it
        // let startThisMonth = DateTime.utc().startOf('month');
        // let params = { from: '2020-01-01T00:00:00Z', to: '2021-02-01T00:00:00Z' };
        // let howManyMonths = Interval.fromDateTimes(DateTime.fromISO(params.from), DateTime.fromISO(params.to)).count('months');
        // startThisMonth = DateTime.fromISO(params.to).startOf('month');

        let durMonth = Duration.fromISO('P1M');
        let startThisMonth = DateTime.utc().startOf('month');

        let startOfMonth = DateTime.fromMillis(this.end+this.interval).toUTC().startOf('month');
        
        // fix to 12 months back from now
        let howManyMonths = 12;

        for(let j=0; j < (howManyMonths); j++) {
            //intervalsDateTime.push(Interval.before(startThisMonth, durMonth));
            
            let timeparams = { start: Interval.before(startThisMonth, durMonth).s.ts, end: Interval.before(startThisMonth, durMonth).e.ts };

            sc = this._standingCharge(timeparams);
            cc = this._consumption( { start: Interval.before(startThisMonth, durMonth).s.toMillis(), end: Interval.before(startThisMonth, durMonth).e.toMillis(), aggregate: Timeseries.AGG_SUM, interval: Duration.fromISO('PT30M') });

            months.push([ Interval.before(startThisMonth, durMonth).s.ts, (sc.cost+cc)]);
            startThisMonth = startThisMonth.minus(durMonth);
        }


        return months;

    }

    /**
     * Total cost for the time period specified
     * includes the standing charge and the 
     * consumption based charges 
     */
    get total() {
        let totalCost = 0;

        totalCost = this.consumption + this.standingcharge.cost;

        return totalCost;
    }

    /**
     * Facade function 
     * Consumption based charges, calculated differently for each
     * tariff type
     */

    get consumption() {
        
        let consumptionCost = 0;

        if(this.tariff.type === 'flat') {
            consumptionCost = this.energy.sum() * this.tariff.flatPrice.rate;
        } else if(this.tariff.type === 'tou') {
            const consumption = this.energy.byHHSlot({aggregate: Timeseries.AGG_SUM});
            const rates = this.tariff._tou;
            for(let i=0; i < 48; i++) {
                consumptionCost += consumption[i] * rates[i];
            }
        } else if(this.tariff.type === 'dynamic') {
            let tariffSlot = parseInt((this.energy.start - this.tariff.start) / this.energy.interval, 10);
            if(tariffSlot > 0) {
                this.energy._rawdata.forEach((consumption) => {
                    //console.log('slot: ' + tariffSlot + ' ' + this.tariff._rawdata[++tariffSlot] + ' * ' + consumption);
                    consumptionCost += this.tariff._rawdata[++tariffSlot] * consumption;
                });
            }
        }

        return (consumptionCost * this._units_multiplier * this._monetary_multiplier);
    }

    /**
     * Facade function for standing charge calculation
     */
    get standingCharge() {
        return this._standingCharge({ start: this.energy.start, end: this.energy.end });
    }



    /**
     * Initialise the cost object with energy and tariff
     * objects.
     * @param {*} energy
     * @param {*} tariff 
     */
    init(data) {
        this.energy = data.energy;
        this.tariff = data.tariff;
        // console.log(this.energy);
    }

    /**
     * Private consumption calculation that can be used to break up time ranges and apply summary functions
     * 
     * @param {*} params 
     */
    _consumption(params) {
        let consumptionCost = 0;

        //console.log(this.energy);
        //console.log({start: params.start, end: params.end, interval: 30*60*1000, aggregate: Timeseries.AGG_SUM});
        let newEnergy = this.energy.range({start: params.start, end: params.end, interval: 30*60*1000, aggregate: Timeseries.AGG_SUM});
        
        if(this.tariff.type === 'flat') {
            consumptionCost = newEnergy.sum() * this.tariff.flatPrice.rate;
        } else if(this.tariff.type === 'tou') {
            const consumption = newEnergy.byHHSlot({aggregate: Timeseries.AGG_SUM});
            const rates = this.tariff._tou;
            for(let i=0; i < 48; i++) {
                consumptionCost += consumption[i] * rates[i];
            }
        } else if(this.tariff.type === 'dynamic') {
            // TBD tariff needs to align
            let tariffSlot = parseInt((newEnergy.start - this.tariff.start) / newEnergy.interval, 10);
            if(tariffSlot >= 0) {
                newEnergy._rawdata.forEach((consumption) => {
                    //console.log('slot: ' + tariffSlot + ' ' + this.tariff._rawdata[++tariffSlot] + ' * ' + consumption);
                    consumptionCost += this.tariff._rawdata[tariffSlot++] * consumption;
                });
            }
        }

        return (consumptionCost * this._units_multiplier * this._monetary_multiplier);
    }

    /**
     * Private function for calculating the standing charges based on number of days
     * @param {*} params 
     */

    _standingCharge(params) {
        // start and end in milliseconds params.start, params.end
        let standingChargeCost = 0;

        let howManyDays = Interval.fromDateTimes(DateTime.fromMillis(params.start), DateTime.fromMillis(params.end)).count('days') - 1;
        standingChargeCost = howManyDays * this.tariff.standingcharge * this._monetary_multiplier;

        return { cost: standingChargeCost, days: howManyDays };
    }

}