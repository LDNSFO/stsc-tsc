'use strict';

/**
 * stsc-tsc v0.4.x | energy.js
 * 
 * Copyright 2020 - 2021 Hildebrand Technology Limit
 * Author: Joshua Cooper - jcooper@hildebrand.co.uk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy o
 * this software and associated documentation files (the "Software"), to deal in th
 * Software without restriction, including without limitation the rights to use
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copie
 * of the Software, and to permit persons to whom the Software is furnishe
 * to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


import axios from 'axios';
import utils from './utils';
import fs from 'fs';
import { DateTime, Duration, Interval, Info } from "luxon";
import * as mTimeseries from './timeseries.js';

const Timeseries = mTimeseries.default || mTimeseries;

/**
 * Energy
 * 
 * This is the main energy class that is used to pull 30 minute energy data
 * there are summarising methods that are made for energy analysis
 * plus the inherited methods of the timeseries as general purpose data manipulation
 */

export default class Energy extends Timeseries {
    
    constructor() {
        
        super();
        /**
         * type is to identify generation, consumption, import, export, submeter
         * fuel can be used to match to tariff (gas, electricity)
         * direction is can be generation, consumption, import, export
         */
        this.type = 'import';
        this.fuel = 'electricity';
        this.direction = 'consumption';

        /**
         * Allows the object to be named
         */
        this.name = 'Imported Electricity';

        this._token = '';
        this._type = 0x01;

        // cached values
        this._cachedProfile = null;

    }

    _intervalDecode(intervalCode) {
        if(intervalCode === 0x0F) return (30 * 60 * 1000);
    }

    /**
     * With the supply of a valid token, load will fetch
     * binary energy data from the server to be used with the 
     * other classes or standalone for running aggregations
     * and analysis
     */

    async load() {
        // web call using token to identify the user
        // then load energy data into structure
        let stscServiceURL = "https://beta.smarttariffsmartcomparison.org/consumption.php";

        if(this.fuel === 'gas') {
            stscServiceURL = "https://beta.smarttariffsmartcomparison.org/gas.php";
            this.name = 'Imported Gas';
        }

        await axios(stscServiceURL, { 
            responseType: 'arraybuffer',
            headers: { 
                'token': this._token
            } 
        } ).then((response) => {

            if(response.status === 200) {
                // this._fetchtime = response.headers.date;
                this._modified = DateTime.utc().ts;

                let ui8 = new Uint8Array(response.data).buffer;
                const view = new DataView(ui8);

                this._type = view.getUint8(0);
                this._rawinterval = view.getUint8(1);
                this.start = view.getUint32(2)*1000;
                
                // get the size
                const numOfElements = parseInt(ui8.byteLength, 10) - 6;
                this._rawdata = new Float64Array(parseInt(numOfElements/2, 10));
                let j = 0;
                for(let i=0; i < numOfElements; i+=2) {
                    this._rawdata[j] = view.getUint16(i+6);
                    j++;
                }

                this.interval = this._intervalDecode(this._rawinterval); // seconds
                this.end = this.start + (this.interval * (j-1));
            }
        })
        .catch((error) => {
            throw new Error(error);
        })
        
    }

    /**
     * Read a binary stsc object from memory
     * 
     */
     _fromBinary(binary) {
        
        // reading from a file
        // ** not working right now :( **)
        // should have a guard function to test for node.js only
        //let data = fs.readFileSync(filename, 'utf-8');
        
        let data = binary;
        let ui8 = new Uint8Array(data).buffer;
        const view = new DataView(ui8);

        this._type = view.getUint8(0);
        this._rawinterval = view.getUint8(1);
        this.start = view.getUint32(2)*1000;
        this._modified = DateTime.utc().ts;
        
        // get the size
        const numOfElements = parseInt(ui8.byteLength, 10) - 6;
        this._rawdata = new Float64Array(parseInt(numOfElements/2, 10));
        let j = 0;
        for(let i=0; i < numOfElements; i+=2) {
            this._rawdata[j] = view.getUint16(i+6);
            j++;
        }

        this.interval = this._intervalDecode(this._rawinterval); // seconds
        this.end = this.start + (this.interval * (j-1)); 
        
    }

    /**
     * Profile summarises the energy data into the main statistics
     * 
     * For ease of use as an object, a histogram and weekly summary is 
     * attached to the object
     */

    profile() {

        if(this._modified > this._cacheModified) {

            let nonzeromin = this.nzmin();
            let min = this.min();
            let max = this.max();
            let mean = this.mean();
            let stddev = this.stddev();
            let histogram = this.histogram();
            let weekly = this.byWeek();

            this._cachedProfile = { 
                nonzeromin: nonzeromin,
                min: min,
                max: max,
                mean: mean,
                stddev: stddev,
                histogram: histogram,
                weekly: weekly
            }

            this._cacheModified = DateTime.utc().ts;

        }

        return {...this._cachedProfile};
    }

    /**
     * Used to extract aggregates from the Energy data
     * there are implementations of some calendar summaries as
     * Luxon does not correctly compute things like P1M on
     * the monthly boundaries
     * 
     * @param from from ISO formatted date time to start
     * @param to ISO formatted date time to stop
     * @param intervalISO ISO interval value for the aggregate to be returned
     * @param aggregate Timeseries aggregate function
     * @param calendar use a fixed calendar to summarise 
     * @returns Timeseries with aggregate values
     */
    consumption(params) {
  
        let series = [];
        let intervalsDateTime = [];
        let aggregateFunction = params.aggregateFunction;

        // default to 30min interval
        let duration = Duration.fromISO('PT30M');
        
        if(params) {
            if(params.intervalISO) {
                duration = Duration.fromISO(params.intervalISO);    
            }
        }

        if(params.calendar) {
            // use for irregular calendar intervals such as January, start of week (Monday)
            // Default time parameters if not specified
            let durationOfAMonth = Duration.fromISO('P1M');
            let startOfMonth = DateTime.utc().startOf('month');
            // default to last 12 months
            let howManyMonths = 12;

            // using start and end parameters and return monthly
            if(params.calendar === 'monthly') {
                // override how many months requested by the from and to parameters if specified
                howManyMonths = Interval.fromDateTimes(DateTime.fromISO(params.from), DateTime.fromISO(params.to)).count('months');
                // If params for "to" was set then start there, not at beginning of timeseries
                startOfMonth = DateTime.fromISO(params.to).startOf('month');
                // build up a set of time intervals for the months of interest
                for(let j=0; j < (howManyMonths-1); j++) {
                    intervalsDateTime.push(Interval.before(startOfMonth, durationOfAMonth));
                    startOfMonth = startOfMonth.minus(durationOfAMonth);
                }
            } else if (params.calendar === 'year') {
                throw new Error('year not implemented yet');
            } else if (params.calendar === 'day' ) {
                throw new Error('day not implemented yet');
            } else {
                // use the defaults and iterate through making the time intervals
                for(let j=0; j < (howManyMonths-1); j++) {
                    intervalsDateTime.push(Interval.before(startOfMonth, durationOfAMonth));
                    startOfMonth = startOfMonth.minus(durationOfAMonth);
                }
            }

            // interate through calendar intervals to build a series
            intervalsDateTime.forEach(intervalDateTime => {
                series = series.concat(
                    this.range(
                        {
                            start: intervalDateTime.start, 
                            end: intervalDateTime.end, 
                            interval: intervalDateTime.end-intervalDateTime.start, 
                            aggregateFunction: aggregateFunction
                        }
                    )
                );
            });

            // change the time order to oldest first
            series.reverse();

        } else {
            // use for the regular intervals - last X days, minutes, hours
            // where the luxon intervals work well
            series = this.range({
                from: params.from, 
                to: params.to, 
                interval: duration, 
                aggregateFunction: aggregateFunction
            });
        }

        return series;
        
    }

    /**
     * Adds energy to the half hour slot specified
     * this can be used for EV additional load
     * @param params 
     */

    addEnergy(params) {
        // amount to spread over start/end slot
        let tsNew = new Energy();
        let outputArray = [];

        tsNew._modified = DateTime.utc().ts;
        tsNew.start = this.start;
        tsNew.end = this.end;
        tsNew.interval = this.interval;
        tsNew._rawinterval = this._rawinterval;

        let slotTs = this.start;
        let daySlot = DateTime.fromMillis(slotTs).weekday - 1;
        let hhSlot = DateTime.fromMillis(slotTs).hour * 2;
        if(DateTime.fromMillis(slotTs).minute > 29) {
            hhSlot++;
        };

        // hhSlot is 0-47 representing the starting slot of the time passed
        // in the THIS timeseries i.e. if timeseries starts at 03:00 then 
        // hhSlot is 6
        
        this._rawdata.forEach((value) => {
            
            if(hhSlot > params.start && hhSlot < params.end && daySlot < 5) {
                outputArray.push([((hhSlot*this.interval) + this.start), value+params.amount]);
            } else {
                outputArray.push([((hhSlot*this.interval) + this.start), value]);
            }
            if(hhSlot < 47) {
                hhSlot++;
            } else {
                hhSlot = 0;
                daySlot = (daySlot < 6) ? (daySlot+1) : 0;
            }
        })

        tsNew.slotdata = outputArray;
        
        return tsNew;
    }

    /**
     * While preserving the overall energy, move an amount of energy from one
     * half hour to another. This is based on percentage in order to not
     * go negative, although it could be made more sophisticated
     * TBD: make this shiftEnergyPercent, create new shiftEnergy with absolute values
     * @param {*} params 
     */
    shiftEnergy(params) {

        // params.from.start = starting slot of where to take energy from
        // params.from.end = stop slot of where to take energy from

        // params.to.start = starting slot of where to put energy
        // params.to.end = stop slot of where to put energy
        // params.amount = % of peak to spread over start/end time

        // model assumes that no energy is lost
        // model assumes that energy during peak hours (16:00 - 18:00)
        // assumes shifting within the same day to earlier in the day

        let tsNew = new Energy();
        let tmpShiftAmount = [];
        let outputArray = [];
        let slot = 0;

        tsNew._modified = DateTime.utc().ts;
        tsNew.start = this.start;
        tsNew.end = this.end;
        tsNew.interval = this.interval;
        tsNew._rawinterval = this._rawinterval;

        let slotTs = this.start;
        let daySlot = DateTime.fromMillis(slotTs).weekday - 1;
        let hhSlot = DateTime.fromMillis(slotTs).hour * 2;
        if(DateTime.fromMillis(slotTs).minute > 29) {
            hhSlot++;
        };

        // hhSlot is 0-47 representing the starting slot of the time passed
        // in the THIS timeseries i.e. if timeseries starts at 03:00 then 
        // hhSlot is 6

        // TBD : beware of first day - if the slot doesn't start at 00:00 there is no
        // where to put it and there will be an error
        
        this._rawdata.forEach((value) => {
            if(hhSlot > params.from.start && hhSlot < params.from.end){
                tmpShiftAmount.push(value);
                outputArray.push([((slot*this.interval) + this.start), 0]);
            } else {
                outputArray.push([((slot*this.interval) + this.start), value]);
            }
            if(hhSlot < 47) {
                hhSlot++;
            } else {
                // crystalise shift of amounts
                // iterate through the from.start to from.end
                let fileSlotFrom = slot - 46 + params.from.start;
                let fileSlotTo = slot - 47 + params.to.start;
                let tmpAmount = 0;
                
                tmpShiftAmount.forEach((amount) => {
                    tmpAmount = Math.floor(amount * params.amount);
                    outputArray[fileSlotFrom][1] = outputArray[fileSlotFrom][1] + (amount - tmpAmount);
                    outputArray[fileSlotTo][1] = outputArray[fileSlotTo][1] + tmpAmount;
                    fileSlotFrom++;
                    fileSlotTo++;
                })
                // reset hh counter and buffer
                hhSlot = 0;
                tmpShiftAmount = [];
            }
            slot++;
        })

        tsNew.data = outputArray;
        
        return tsNew;
    }


    /**
     * Big, maybe too big, analysis of behavioural parameters on a week by week basis
     * mostly used in profile
     * TBD: check if there are any issues around localtime
     * @param {*} params 
     */
    byWeek(params) {
        // returns up 52 weeks (?) of parameters

        let reducedData = [];
        
        // this will be a 3D array of 7 x 48 x 52 weeks in the data set
        let workingData = [];

        for(let i=0; i < 53; i++) {
            workingData[i] = [];
            reducedData[i] = [];
            for(let j=0; j < 7; j++) {
                workingData[i][j] = [];
                for(let k=0; k < 48; k++) {
                    workingData[i][j][k] = null;
                }
                reducedData[i][j] = 0;
            }
        }
        
        let slotTs = this.start;
        let daySlot = DateTime.fromMillis(slotTs).weekday - 1;
        let weekSlot = DateTime.fromMillis(slotTs).weekNumber - 1;
        let hhSlot = DateTime.fromMillis(slotTs).hour * 2;
        if(DateTime.fromMillis(slotTs).minute > 29) {
            hhSlot++;
        };

        this._rawdata.forEach((value) => {
            workingData[parseInt(weekSlot, 10)][parseInt(daySlot, 10)][parseInt(hhSlot, 10)] = value;
            slotTs += this.interval;
            daySlot = DateTime.fromMillis(slotTs).weekday - 1;
            weekSlot = DateTime.fromMillis(slotTs).weekNumber - 1;
            if(hhSlot < 47) {
                hhSlot++;
            } else {
                hhSlot = 0;
            }
        });

        // use slot zero for the weekly in case there is more than one year

        let avDaily = [];
        let varDaily = [];
        let avBaseline = [];
        let varBaseline = [];
        let avWeekDay = [];
        let varWeekDay = [];
        let avWeekEnd = [];
        let varWeekEnd = [];

        let avDayUse = [];
        let varDayUse = [];
        let avEveningUse = [];
        let varEveningUse = [];
        let avOffPeakUse = [];
        let varOffPeakUse = [];

        let avProfile = [];

        for(let i=0; i < 53; i++) {
            let tmpDailySum = [];
            let tmpDailyMin = [];
            let tmpWkDaySum = [];
            let tmpWkDayMin = [];
            let tmpWkEndSum = [];
            let tmpWkEndMin = [];
            let tmpDayUse = [];
            let tmpEveningUse = [];
            let tmpOffPeakUse = [];

            for(let j=0; j < 7; j++) {
                let dailySum = 0;
                tmpDailySum.push(utils._sum(workingData[i][j]));
                tmpDailyMin.push(utils._nzmin(workingData[i][j]));
                if(j < 5) {
                    tmpWkDaySum.push(utils._sum(workingData[i][j]));
                    tmpWkDayMin.push(utils._nzmin(workingData[i][j]));
                } else {
                    tmpWkEndSum.push(utils._sum(workingData[i][j]));
                    tmpWkEndMin.push(utils._nzmin(workingData[i][j]));
                }
                tmpDayUse.push(this._extractSlot({from: 15, to: 31}, workingData[i][j]));
                tmpEveningUse.push(this._extractSlot({from: 32, to: 47}, workingData[i][j]));
                tmpOffPeakUse.push(this._extractSlot({from: 0, to: 14}, workingData[i][j]));
                //console.log(utils._sum(workingData[i][j]));
            }
            avDaily[i] = utils._mean(tmpDailySum);
            varDaily[i] = utils._variance(tmpDailySum);
            avBaseline[i] = utils._mean(tmpDailyMin);
            varBaseline[i] = utils._variance(tmpDailyMin);
            avWeekDay[i] = utils._mean(tmpWkDaySum);
            varWeekDay[i] = utils._variance(tmpWkDaySum);
            avWeekEnd[i] = utils._mean(tmpWkEndSum);
            varWeekEnd[i] = utils._variance(tmpWkEndSum);
            avDayUse[i] = utils._mean(tmpDayUse);
            varDayUse[i] = utils._variance(tmpDayUse);
            avEveningUse[i] = utils._mean(tmpEveningUse);
            varEveningUse[i] = utils._variance(tmpEveningUse);
            avOffPeakUse[i] = utils._mean(tmpOffPeakUse);
            varOffPeakUse[i] = utils._variance(tmpOffPeakUse);

            avProfile[i] = {
                 averageDaily: avDaily[i], 
                 varianceDaily: varDaily[i],
                 averageBaseline: avBaseline[i],
                 varianceBaseline: varBaseline[i],
                 averageWeekDay: avWeekDay[i],
                 varianceWeekDay: varWeekDay[i],
                 averageWeekEndDay: avWeekEnd[i],
                 varianceWeekEndDay: varWeekEnd[i],
                 averageDayUse: avDayUse[i],
                 varianceDayUse: varDayUse[i],
                 averageEveningUse: avEveningUse[i],
                 varianceEveningUse: varEveningUse[i],
                 averageOffPeakUse: avOffPeakUse[i],
                 varianceOffPeakUse: varOffPeakUse[i]
            }
        }

        return avProfile;

    }

    _extractSlot(params, list) {
        // from, to slot in params - data in list
        // assume params.to is not bigger than list size
        // assume params.from is greater than or equal to 0
        let acc = 0;
        list.forEach((value, i) => {
            if(i >= params.from && i <= params.to) {
                acc += value;
            }
        });
        return acc;
    }


}
