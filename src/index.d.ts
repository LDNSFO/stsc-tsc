/**
 * Index for types
 * Types used across the library
 *
 * Copyright 2020 - 2021 Hildebrand Technology Limit
 * Author: Joshua Cooper - jcooper@hildebrand.co.uk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy o
 * this software and associated documentation files (the "Software"), to deal in th
 * Software without restriction, including without limitation the rights to use
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copie
 * of the Software, and to permit persons to whom the Software is furnishe
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * Hand coded export of the main types used
 * some of the transpiling of Javascript to Typescript will
 * create its own types
 */

export class Timeseries {
  start: number;
  end: number;
  interval: number;
  byMonth: Array<Coordinates>;

  data(data: Array<Coordinates>): Timeseries;
  range(param: rangeOptions): Timeseries;
  slotdata(data: Array<number>): Timeseries;

  count(): number;
  nzmin(): number;
  max(): number;
  mean(): number;
  median(): number;
  min(): number;
  stddev(): number;
  sum(): number;
  variance(): number;

  scale(a: number);
  translate(b: number);

  export(): Array<Coordinates>;
  histogram(): Array<number>;
  byHHSlot(param: aggregateOptions): Array<number>;
  byDOW(param: aggregateOptions): Array<number>;
  byHHSlotDOW(param: aggregateOptions): Array<Coordinates>;

  AGG_SUM(): number;
  AGG_MEAN(): number;
  AGG_VAR(): number;
  AGG_MAX(): number;
  AGG_MIN(): number;
  AGG_COUNT(): number;
  AGG_STDDEV(): number;
  AGG_VELOCITY(): number;
  AGG_ACCELERATION(): number;

  _modified: number;
  _rawdata: Array<number>;
  _rawinterval: number;
  _fetchtime: number;
  _type: number;

  _decodeAggregate(code: number): any;
  _range(param: rangeOptions): Timeseries;
  _twoDData(): Array<Triples>;
}

export interface rangeOptions {
  start: string;
  end: string;
  interval: number;
  validate?: boolean;
}

export interface aggregateOptions {
  aggregate: string;
}

export type Coordinates = Array<number>;
export type Triples = Array<number>;

export class Energy extends Timeseries {
  load(): Promise<void>;
  profile(): object;
  consumption(param: rangeOptions): Energy;
  clearPrediction(): void;
  addEnergy(param: object): Energy;
  shiftEnergy(param: object): Energy;
  epc(param: object): object;
  weekly(param: object): Array<object>;

  _token: string;

  _intervalDecode(code: number): number;
  _extractSlot(param: rangeOptions, list: Array<number>): number;
}

export interface touPrice {
  valid_from: string;
  valid_to: string;
  value_inc_vat: number;
}

export class Tariff extends Timeseries {
  id: string;
  name: string;
  supplier: string;
  description: string;
  product: string;
  productId: string;
  type: string;
  gsp: string;
  _flat: {
    rate: number;
  };
  _tou: Float64Array;
  exitFees: number;
  contractLength: number;
  green: boolean;
  fuel: boolean;
  standingCharge: number;
  loadDynamic(params: any): Promise<void>;
  set flatPrice(arg: { start: any; rate: number; standingCharge: any });
  get flatPrice(): {
    start: any;
    rate: number;
    standingCharge: any;
  };
  set touPrice(arg: Array<touPrice>);
  get touPrice(): Array<touPrice>;
  get dynamicPrice(): Float64Array;
  setTariff(tariff: any): Promise<void>;
  _intervalDecode(intervalCode: any): number | undefined;
  _touSetSlotPriceWithDate(touPrices: any): void;
  _touSetSlotPriceWithFullDate(touPrice: any): void;
  _touSetSlotPriceWithHour(data: any): void;
  _touSetSlotPrice(data: any): void;
  _touEncode(): Array<touPrice>;
}
