/**
 * STSC
 * Entry class that makes working with server objects personalised
 *
 * Copyright 2020 - 2021 Hildebrand Technology Limit
 * Author: Joshua Cooper - jcooper@hildebrand.co.uk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy o
 * this software and associated documentation files (the "Software"), to deal in th
 * Software without restriction, including without limitation the rights to use
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copie
 * of the Software, and to permit persons to whom the Software is furnishe
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import axios from 'axios';
import { Suppliers, Products, Energy } from '.';

/**
 * Username and password strings passed to login method
 * @interface LoginOptions
 * @desc Username and password strings passed to login method
 */
export interface LoginOptions {
  username: string;
  password: string;
}

export default class STSC {
  public token: string;
  private _baseURL: string;
  private VERSION: string;

  private _modified: number = 0;

  public gsp: string = '';
  public epc: any = {};

  public suppliers: any = [];
  public products: any = [];
  public predictedEnergy: any = new Energy();

  constructor() {
    this.token = '';
    // the base URL for the Smart Tariff application
    this._baseURL = 'https://beta.smarttariffsmartcomparison.org/';
    this.VERSION = '1.0.9';
  }

  /**
   * Provides a token from the main Glow service for access to
   * meter data, tariffs, etc
   *
   * Token is saved in token property of this object
   *
   * See Glowmarkt APIs for full explaination, however only a
   * this one call is needed - or you can get the token directly
   * from calling the Glow platform login or cut and paste from user
   *
   * @param param LoginOptions
   *
   */

  async login(param: LoginOptions) {
    // authentication is from the Glowmarkt systems that are APIs
    // seperate from the smart tariff application
    const stscServiceURL = 'https://api.glowmarkt.com/api/v0-1/auth/';

    await axios
      .post(
        stscServiceURL,
        {
          username: param.username,
          password: param.password,
        },
        {
          headers: {
            // the smart tariff application Id as it is known to Glowmarkt
            applicationId: '6dc7e58b-4f10-4897-9a6b-a0971bee1235',
          },
        },
      )
      .then((response) => {
        if (response.status === 200) {
          this.token = response.data.token;
          return response.data;
        }
      })
      .catch((err) => {
        // console.log(err);
      });
  }

  /**
   * Returns the estimated time that it will take to load
   * data from all sources, can take a while given
   * the nature of the remote data sources
   *
   * token must be set or there will be an error back from
   * the API
   *
   * @returns JSON string of loadtime parameters from server
   *
   */

  async loadtime() {
    const stscServiceURL = this._baseURL + 'loadtime.php';

    await axios(stscServiceURL, {
      headers: {
        'Cache-Control': 'no-store',
        token: this.token,
      },
    }).then((response) => {
      if (response.status === 200) {
        this.loadtime = response.data;
        return response.data;
      }
    });
  }

  /**
   * destroys server side data and flushes cache
   * token becomes unusable
   */

  async logout() {
    const stscServiceURL = this._baseURL + 'delete.php';

    await axios(stscServiceURL, {
      headers: {
        'Cache-Control': 'no-store',
        token: this.token,
      },
    }).then((response) => {
      if (response.status === 200) {
        this.clearToken();
        return response.data;
      }
    });
  }

  /**
   *
   * server side refresh of any data held in server cache
   * this is good as a failsafe in case there was a delay
   * in data construction that resulted in an incomplete cache
   */

  async flushdata() {
    const stscServiceURL = this._baseURL + 'flush.php';

    await axios(stscServiceURL, {
      headers: {
        'Cache-Control': 'no-store',
        token: this.token,
      },
    }).then((response) => {
      if (response.status === 200) {
        return response.data;
      }
    });
  }

  /**
   * From the token, give back the GSP code that can be used for other queries
   */

  async loadgsp() {
    const stscServiceURLGSP = this._baseURL + '/gsp.php';

    await axios(stscServiceURLGSP, {
      headers: {
        'Cache-Control': 'no-store',
        token: this.token,
      },
    })
      .then((response) => {
        if (response.status === 200) {
          this.gsp = response.data.gsp;
        }
      })
      .catch((error) => {
        throw new Error(error);
      });
  }

  /**
   * Load EPC for the energy based on the assumption that the MPAN is being used
   * and the server knows which EPC to retun based on the login token
   * @param {*} params
   */
  async loadepc() {
    // token will match the address
    const stscServiceURL = 'https://beta.smarttariffsmartcomparison.org/epc.php';

    await axios(stscServiceURL, {
      responseType: 'arraybuffer',
      headers: {
        token: this.token,
      },
    })
      .then((response) => {
        if (response.status === 200) {
          this.epc = response.data;
        }
      })
      .catch((error) => {
        throw error;
      });
  }

  /**
   * Set suppliers that are in context, usually called before a load
   */

  setSuppliers(suppliers: any) {
    this.suppliers = suppliers;
  }

  /**
   * Set products that are in context, usually called after a load of the products
   */

  setProducts(products: any) {
    this.products = products;
  }

  /**
   * Set predictedEnergy
   */

  setPredictedEnergy(predictedEnergy: Energy) {
    this.predictedEnergy = predictedEnergy;
  }

  /**
   * Clears the token
   */

  clearToken() {
    this.token = '';
  }
}
