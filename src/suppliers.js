'use strict';

/**
 * Suppliers
 * Class that loads Supplier information - sourced from Citizens Advice
 * then anonymised for the project
 * 
 * Copyright 2020 - 2021 Hildebrand Technology Limit
 * Author: Joshua Cooper - jcooper@hildebrand.co.uk
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy o
 * this software and associated documentation files (the "Software"), to deal in th
 * Software without restriction, including without limitation the rights to use
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copie
 * of the Software, and to permit persons to whom the Software is furnishe
 * to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import axios from 'axios';
import { DateTime, Duration, Interval, Info } from "luxon";

export default class Suppliers {
    
    constructor() {
        this._baseURL = 'https://beta.smarttariffsmartcomparison.org/';
        this.suppliers = [];
        this.token = '';
    }

    /**
     * Load the suppliers from the server
     */

    async load() {

        let stscServiceURL = this._baseURL + '/suppliers.php';         

        await axios(stscServiceURL, { 
            headers: { 
                'token': this.token
            } 
        } ).then((response) => {
            if(response.status === 200) {
                this._modified = response.headers['last-modified'];
                this.suppliers = response.data;   
            }
        })
        .catch((error) => {
            throw new Error(error);
        })
        
    }


}
